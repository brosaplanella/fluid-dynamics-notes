import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.optimize as so

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'


Lx = 3
Ly = 3
Rmax = 10
Nx = 40
Ny = 40
Nr = 400
Nt = 400
x = np.linspace(-Lx, Lx, Nx)
y = np.linspace(-Ly, Ly, Ny)
r = np.linspace(1, Rmax, Nr)
t = np.linspace(1e-3, 2*np.pi-1e-3, Nt)

X, Y = np.meshgrid(x,y)
R, T = np.meshgrid(r,t)
Z = X+1j*Y
Zr = R*np.exp(1j*T)

fig = plt.figure(1, figsize=(6.5,4))
fig.clf()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212, sharex=ax1)
# ax3 = fig.add_subplot(313, sharex=ax1)
ax1.axis('scaled')
ax2.axis('scaled')
# ax3.axis('scaled')

alpha = 30*np.pi/180
eia = np.exp(-1j*alpha)
Gamma = -4*np.pi*np.sin(alpha)
epsilon = 0.0
W = Zr*eia + 1/(Zr*eia) - 1j*Gamma/(2*np.pi)*(np.log(-Zr) - 1j*np.pi)
X = Zr.real
Y = Zr.imag

Zeta = (1+epsilon)*Zr + (1-epsilon)/Zr

rc = 1 + 0*t
philevels = np.linspace(-3*np.pi, 5*np.pi, 25)
psilevels = np.linspace(-2*np.pi, 3*np.pi, 61)

ax1.contour(X, Y, W.real, levels=philevels, colors='r', linewidths=0.5)
ax1.contour(X, Y, W.imag, levels=psilevels, colors='b', linewidths=0.5)
ax1.contour(X, Y, W.imag, levels=[0], colors='k', linewidths=1)
ax1.fill(rc*np.cos(t), rc*np.sin(t))
ax1.plot(rc*np.cos(t), rc*np.sin(t), 'k-')

ax2.contour(Zeta.real, Zeta.imag, W.real, levels=philevels, colors='r', linewidths=0.5)
ax2.contour(Zeta.real, Zeta.imag, W.imag, levels=psilevels, colors='b', linewidths=0.5)
ax2.contour(Zeta.real, Zeta.imag, W.imag, levels=[0], colors='k', linewidths=1)
ax2.fill(2*rc*np.cos(t), 2*epsilon*rc*np.sin(t))
ax2.plot(2*rc*np.cos(t), 2*epsilon*rc*np.sin(t), 'k-')

ax1.set_xlim([-8,8])
ax1.set_ylim([-2.5,2.5])
# ax2.set_xlim([-6,6])
ax2.set_ylim([-2.5,2.5])
# ax3.set_ylim([-2.5,2.5])
plt.setp(ax1.get_xticklabels(), visible=False)
# plt.setp(ax2.get_xticklabels(), visible=False)

ax1.set_xlabel('$\\xi$')
ax1.set_ylabel('$\\eta$')
ax2.set_xlabel('$x$')
ax2.set_ylabel('$y$')

ax1.text(-7.7, -2.3, '(a)')
ax2.text(-7.7, -2.3, '(b)')
# ax3.text(-7.7, -2.3, '(c)')

plt.show(block=False)
fig.savefig('../FlatPlate2.pdf')
fig.savefig('../FlatPlate2.svg')

