import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'


Lx = 3
Ly = 3
Rmax = 10
Nx = 40
Ny = 40
Nr = 40
Nt = 40
x = np.linspace(-Lx, Lx, Nx)
y = np.linspace(-Ly, Ly, Ny)
r = np.linspace(1e-2, Rmax, Nr)
t = np.linspace(0, 2*np.pi-1e-3, Nt)

X, Y = np.meshgrid(x,y)
R, T = np.meshgrid(r,t)
Z = X+1j*Y
Zr = R*np.exp(1j*T)

fig = plt.figure(1, figsize=(6.5,4))
fig.clf()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212, sharex=ax1)

Q = 0.5
W = Zr + Q*np.log(-Zr)
X = Zr.real
Y = Zr.imag

rc = t.copy()
rc[1:] = Q*(np.pi-t[1:])/np.sin(t[1:])
rc[0] = rc[-1]

philevels = np.linspace(-6, 8, 31)
psilevels = np.linspace(-3, 3, 31)
ax1.contour(X, Y, W.real, levels=philevels, colors='r', linewidths=0.5)
ax1.contour(X, Y, W.imag, levels=psilevels, colors='b', linewidths=0.5)
# ax1.contour(X, Y, W.imag, levels=[0], colors='b', linewidths=0.5)

ax2.contour(X, Y, W.real, levels=philevels, colors='r', linewidths=0.5)
ax2.contour(X, Y, W.imag, levels=psilevels, colors='b', linewidths=0.5)
# ax2.contour(X, Y, W.imag, levels=[0], colors='k', linewidths=2)
ax2.fill(rc*np.cos(t), rc*np.sin(t))
ax2.plot(rc*np.cos(t), rc*np.sin(t), 'k-')

ax1.set_xlim([-6,6])
ax1.set_ylim([-3,3])
# ax2.set_xlim([-6,6])
ax2.set_ylim([-3,3])
plt.setp(ax1.get_xticklabels(), visible=False)

ax2.set_xlabel('$x$')
ax2.set_ylabel('$y$')

ax1.text(-5.7, -2.8, '(a)')
ax2.text(-5.7, -2.8, '(b)')

plt.show(block=False)
fig.savefig('../Figures/Rankine.pdf')
fig.savefig('../Figures/Rankine.svg')

