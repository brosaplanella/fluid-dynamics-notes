import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'


ids = np.hstack((np.array([0,1]), np.arange(2,403,20)))
d = np.loadtxt('SoundPressure.txt.bz2', comments='%', usecols=ids)
x = d[:,0]
y = d[:,1]
d = d[:,2:]

Nx = 101
Ny = 11
x = x.reshape((Ny,Nx))
y = y.reshape((Ny,Nx))

dNx = 1
dNy = 1
x = x[::dNy,::dNx]
y = y[::dNy,::dNx]

Nt = ids.shape[0]-2
Nt = 18
fig = plt.figure(1, figsize=(2,9))
fig.clf()
cmap = plt.get_cmap('RdBu')
axes = []
for ii in range(0,Nt,2):
    ax = fig.add_subplot(int(Nt/2),1,int(ii/2)+1)
    p = 1e3*(d[:,ii].reshape((Ny,Nx))-1) 
    im = ax.pcolormesh(x, y, p[::dNy,::dNx], cmap=cmap, vmax=2, vmin=-2)
    axes.append(ax)

for ax in axes[:-1]:
    ax.set_xticklabels([])
    ax.set_yticklabels([])

axes[-1].set_xlabel('$x$')
axes[-1].set_ylabel('$y$')

fig.tight_layout()
fig.subplots_adjust(top=0.94, hspace=0.05, wspace=0.05)
cbar_ax = fig.add_axes([0.1, 0.96, 0.8, 0.01])
fig.colorbar(im, cax=cbar_ax, orientation='horizontal')

# plt.show(block=False)
fig.savefig('../Figures/SoundPressure.pdf')
fig.savefig('../Figures/SoundPressure.svg')

