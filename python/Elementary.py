import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'


Lx = 3
Ly = 3
Rmax = 5
Nx = 40
Ny = 40
Nr = 40
Nt = 40
x = np.linspace(-Lx, Lx, Nx)
y = np.linspace(-Ly, Ly, Ny)
r = np.linspace(1e-2, Rmax, Nr)
t = np.linspace(0, 2*np.pi-1e-3, Nt)

X, Y = np.meshgrid(x,y)
R, T = np.meshgrid(r,t)
Z = X+1j*Y
Zr = R*np.exp(1j*T)

fig = plt.figure(1, figsize=(4,4))
fig.clf()
ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)

xc = 2*np.cos(t)
yc = 2*np.sin(t)

alpha = np.pi/6
W1 = Z*np.exp(-1j*alpha)
W2 = np.log(-Zr) - 1j*np.pi
W3 = 1j*np.log(-Zr) - 1j*np.pi
W4 = 1/Z

ax1.contour(X, Y, W1.real, 20, colors='r', linewidths=0.5)
ax1.contour(X, Y, W1.imag, 20, colors='b', linewidths=0.5)

ax2.contour(Zr.real, Zr.imag, W2.real, 20, colors='r', linewidths=0.5)
ax2.contour(Zr.real, Zr.imag, W2.imag, 20, colors='b', linewidths=0.5)
ax2.plot(xc, yc, 'k-')

ax3.contour(Zr.real, Zr.imag, W3.real, 20, colors='r', linewidths=0.5)
ax3.contour(Zr.real, Zr.imag, W3.imag, 20, colors='b', linewidths=0.5)
ax3.plot(xc, yc, 'k-')

ax4.contour(X, Y, W4.real, levels=np.sort(1.0/np.linspace(6,-6,20)), colors='r', linewidths=0.5)
ax4.contour(X, Y, W4.imag, levels=np.sort(1.0/np.linspace(6,-6,20)), colors='b', linewidths=0.5)

ax2.set_xlim([-3,3])
ax2.set_ylim([-3,3])
ax3.set_xlim([-3,3])
ax3.set_ylim([-3,3])

ax3.set_xlabel('$x$')
ax3.set_ylabel('$y$')

ax1.text(-2.9,-2.9, '(a)')
ax2.text(-2.9,-2.9, '(b)')
ax3.text(-2.9,-2.9, '(c)')
ax4.text(-2.9,-2.9, '(d)')

# plt.show(block=False)
plt.savefig('../Figures/Elementary.pdf')
plt.savefig('../Figures/Elementary.svg')

