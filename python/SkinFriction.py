import numpy as np

d = np.loadtxt('SkinFriction.txt', comments='%')
U = d[:,0]
mu = np.array([1e-5, 3e-5, 5e-5])
D = d[:,1:]

L = 0.05
rho = 1
fpp = 0.4695999884592487
Mu, UU = np.meshgrid(mu, U)

