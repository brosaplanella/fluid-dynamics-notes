import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'

d = np.loadtxt('LiftCoefficient.txt', comments='%')
alpha = d[:,0]
CL = d[:,1]
arad = alpha*np.pi/180
CLtheory = 2*np.pi*np.sin(arad)

fig = plt.figure(1, figsize=(3,3))
fig.clf()
ax = fig.add_subplot(111)

ax.plot(alpha, CL, 'o')
ax.plot(alpha, CLtheory, 'k-')
ax.set_xlabel('$\\alpha$ ($^\circ$)')
ax.set_ylabel('$C_L$')
fig.tight_layout()
plt.show(block=False)
fig.savefig('../Figures/LiftCoefficient.pdf')
fig.savefig('../Figures/LiftCoefficient.svg')

