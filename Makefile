SHELL = bash
HTMLDIR = html
html: pdf html/book.html
	echo "Inside Makefile make html"
	echo ${CI_COMMIT_BRANCH}
	mkdir -p ${HTMLDIR}
	mv book.pdf ${HTMLDIR}
	rm -rf public/${HTMLDIR}
	mv ${HTMLDIR} public

pdf:
	TZ=Europe/London date > public/last_update.txt
	pdflatex book.tex && pdflatex book.tex && pdflatex book.tex && pdflatex book.tex

html/book.html:	
	latexml --dest=book.xml book
	mkdir -p ${HTMLDIR}
	latexmlpost -css=css/LaTeXML-navbar-left.css --split --splitat=chapter --navigationtoc=context --dest=${HTMLDIR}/book.html book.xml
	./loopgtag.sh ${HTMLDIR}


test:
	echo ${HTMLDIR}
