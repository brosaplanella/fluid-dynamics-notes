\chapter{Preliminaries}
The theory of fluid dynamics rests on the developments in some greater disciplines such as mathematics and physics, especially mechanics. 
In this chapter, let us summarize and revise some of these topics.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Newtonian mechanics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
While it is not possible to do justice to Newtonian mechanics here, we present the basic tenets.
\footnote{For more detailed treatments, refer to a book on undergraduate mechanics, e.g., Halliday and Resnick, Physics - I, or, if you prefer the economy of Soviet authors, I. E. Irodov, Fundamental Laws of Mechanics.}
Newtonian mechanics, at its core, describes physical laws governing the motion of point material particles.
Consider a point material object of mass $m$ moving through empty space.
Let us say that the objects location is given by $\bx(t)$ as a function of time $t$.
The velocity of the body is $\bv(t) = d\bx/dt$.
The three laws of motion according to Newtonian mechanics are:
\begin{enumerate}
 \item {\bf First law:} In an inertial reference frame, every body continues in its state of rest or uniform motion until and unless acted upon by an external unbalanced force. Essentially, this law defines the notion of an inertial reference frame, i.e. one in which a free body moves uniformly. 
 \item {\bf Second law:} When an external unbalanced force, $\bff$, acts on a body it must equal the rate of change of momentum of this body. The momentum (also known as the linear momentum) of this point mass is defined as $\bp = m \bv$. Therefore, the mathematical statement of the second law is:
 \begin{align}
  \bff = \dfrac{d\bp}{dt}.
 \end{align}
Noting that the acceleration of the particle, $\ba = d\bv/dt =$, if the mass of the body does not change with time, then this law is also written as $\bff = m\ba$.
This law describes the motion of the particle.
 \item {\bf Third law:} Every action has an equal and opposite reaction. In other words, if a body $A$ exerts a force $\bff$ on a body $B$, then the body $B$ must exert a force of $-\bff$ on body $A$. This law describes the interaction between particles.
\end{enumerate}

As a consequence of Newton's laws of motion for point particles, it is possible to derive the following ``conservation laws'' of momentum and angular momentum for extended deformable bodies.
A common terminology used in physics is the moment of a quantity, which is obtained by making a cross product of the position vector $\bx$ of a point particle with that quantity. 
Suppose that an extended material object at time $t$ occupies a region of space given by $\Omega$.
Then its total momentum is obtained by breaking it up into infinitesimal pieces of mass $dm$, determining the momentum of these individual pieces, and summing the momentum.
Mathematically, this amounts to integrating as
\begin{align}
 \bp = \int_\Omega dm~\bv(\bx, t),
\end{align}
where $\bv$ is the velocity of the piece at position $\bx\in\Omega$.
The total mass $m$ of the body is simply $m = \int_\Omega dm$.
The moment of momentum , also known as the angular momentum $\bl$, is then
\begin{align}
 \bl = \int_\Omega dm~\bx \times \bv.
\end{align}

These conservation laws invoke the ``centre of mass'' $\bx_c$ of this object, defined as
\begin{align}
 \bx_c = \dfrac{\int_\Omega dm~\bx}{\int_\Omega dm} = \dfrac{1}{m} \int_\Omega dm~\bx.
\end{align}
Suppose that the external forces $\bff_i$, $i=1,2,\dots,n$ act on this object at locations $\bx_i$.
The net torque $\btau$ on this body is the moment of the external forces,
\begin{align}
 \btau = \sum_{i=1}^n \bx_i \times \bff_i.
\end{align}

So long as the object does not exchange material with anything outside it, the laws of conservation state that:
\begin{enumerate}
 \item {\bf Conservation of (linear) momentum:} The rate of change of total momentum must equal the total external unbalanced force on the object, i.e.
 \begin{align}
  \dfrac{d\bp}{dt} = \sum_{i=1}^n \bff_i.
 \end{align}
 Noting the relation between $\bp$ and $\bx_c$, this law is also stated as
 \begin{align}
  m\dfrac{d^2\bx_c}{dt^2} = \sum_{i=1}^n \bff_i,
 \end{align}
 which means that the mass times the acceleration of the centre of mass must equal the external unbalanced force.
 \item {\bf Conservation of angular momentum:} The rate of change of angular momentum must equal the net torque on the body. Mathematically,
 \begin{align}
  \dfrac{d\bl}{dt} = \btau.
 \end{align}
\end{enumerate}
While the momentum and angular momentum are not constants of motion, here the term ``conservation law'' is used in the sense that it is possible to keep an account of these quantities in relation to their respective sources.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Units and dimensions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Units are used to describe the values of physical quantities. For example, Dr. Shreyas Mandre is 170 cm tall, weigh 10 stones and consume about 100 Watts when resting. The choice of the unit is arbitrary, as reflected in the plethora of units available for the same quantity, e.g. 170 cm $\approx$ 68 inches $\approx$ 1.8 $\times 10^{-16}$ light-years. The SI system is popular for the purpose of standardization (and that purpose alone). 

The fundamental nature of a quantity attributes it with a unique dimension. Example of dimensions are length $L$, mass $M$, time $T$, force $F$, etc. Many different units are associated with each dimension, as described above. The basic rules for analysis based on dimensions are as follows. The dimension of product of physical quantities is the product of the dimensions of the individual multiplicands. It is an error to add quantities of different dimensions, i.e. lengths may be added with lengths but not mass. For an equation in physics to be dimensionally consistent, the dimensions on both sides of the equation must be identical.

Dimensions of quantities are not necessarily independent of each other. For example, the dimensions of volume $L^3$ depend on the dimensions of length. This dependence is best expressed in systems of dimensions. In the $MLT$ system, the dimensions $M$, $L$ and $T$ are considered basic, from which all other dimensions are derived, e.g. $F=ML/T^2$. There is some arbitrariness in constructing systems of dimensions, for example, equally valid is the $FLT$ system where $M = FT^2/L$ is a derived dimension. 

\noindent
{\bf Exercise}: Construct a $\rho VL$ system, where $\rho$ is the dimension of density and $V$ of velocity. Express the dimensions of mass and time in terms of $\rho$, $V$ and $L$.

\noindent
{\bf Exercise}: What are the dimensions of $p$, $\mu$ and $\nu$?

This arbitrariness in the nature of physical quantities underlies some of the most powerful theoretical tools in physics. It was on the basis of such considerations that Sir G. I. Taylor estimated the yield of the atomic bomb, a heavily guarded American secret at the time, from publicly available images. Ignore dimensions at your own peril!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cartesian tensors}  \label{sec:carttens}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Vectors and tensors offer a compact representation to tackle the large number of variables whilst accounting for the relationship between them. Vectors are quantities that, in the colloquial sense, have a magnitude and a direction. Mathematically, vectors are elements of $\mathbb{R}^3$, where the components commonly represent Cartesian components.  Tensors are generalization of vectors which have more than three components in 3-dimensions.  An $n^\text{th}$ rank tensor has $3^{n}$ components in 3 dimensions. In this way, a vector is a first-rank tensor. Examples of second rank tensors include moment of inertia of a solid body. Here the nine Cartesian components of the moment-of-inertia tensor are
\begin{align*}
 &I_{xx} = \int_V x^2~dm,& \qquad
 &I_{yy} = \int_V y^2~dm,& \qquad
 &I_{zz} = \int_V z^2~dm,& \\
 I_{yx} = &I_{xy} = \int_V xy~dm,& \qquad
 I_{zy} = &I_{yz} = \int_V yz~dm,& \qquad
 I_{zx} = &I_{xz} = \int_V xz~dm,&
\end{align*}
where $dm$ represents an infinitesimal mass element spanning the extent of the body $V$.

Tensors of rank zero are scalars.

Two different notations are available to represent vector and tensor expressions, they are called the vector notation and the index notation. Depending on the circumstance, one notation may be preferred to the other either for clarity or compactness.

We will use boldface symbols to represent vectors and tensors in vector notation. In this notation, it is crucial to declare the rank of the tensor a symbol represents, e.g., let $\bu$ represent first-rank velocity tensor, and let $\bT$ represent the second rank stress tensor. The operations on these symbols are understood as a matter of convention, e.g., $\ba + \bb$ is the sum of two tensors $\ba$ and $\bb$ of equal rank. Vector notation is not equipped to unambiguously represent dot products and cross products, but it is denoted as $\ba\cdot\bb$ and $\ba \times \bb$, respectively, when no ambiguity exists. Examples will be abundant in the rest of the notes. The advantage of this notation is that it treats tensors in their abstraction without reference to any specific coordinate system.

The index notation explicitly invokes (for this module Cartesian) coordinate system. The Cartesian coordinates are themselves written as $x_1 = x$, $x_2 = y$ and $x_3 = z$. Let $\e_1$, $\e_2$ and $\e_3$ be the unit vectors along $x_1$, $x_2$ and $x_3$ coordinates, respectively. Then the components $u_1$, $u_2$ and $u_3$ of the tensor a first rank tensor (i.e., a vector) $\bu$ are written as $u_i$, where the index $i$ ranges from 1 to 3. The components of a second rank tensor are represented using two indices, e.g., the components of the moment-of-inertia tensor are represented as $I_{ij}$, where 
\begin{align*}
 I_{ij} = \int_V x_ix_j~dm, \quad \text{and $i$ and $j$ both independently range from 1 to 3. }
\end{align*}
It is customary to omit the range of the indices in this notation. In general, $n^\text{th}$ rank tensors have $n$ indices.

{\bf The summation convention: }
Expressions, such as the dot product between two vectors are written as a sum in index notation, e.g.
\begin{align}
\ba \cdot \bb = a_1b_1 + a_2b_2 + a_3b_3 = \sum_{i=1}^3  a_i b_i.
\end{align}
As a matter of convention due to Einstein, it is customary to also omit the summation sign over an index which appears twice in any product. Hence, $\ba \cdot \bb = a_i b_i$, with an implied sum over $i$ from 1 to 3. When a sum on an index appearing twice is not implied, this must be stated explicitly. Similarly, whether a sum is implied on an index that repeats more than two times must be explicitly stated.

{\bf Two special tensors: } 
Let us introduce two special and basic tensors. The first is called the Kronecker delta, defined as
\begin{align}
\delta_{ij} = \begin{cases}
\quad 0& \text{for }i\neq j\\
\quad 1& \text{for }i=j\\
\end{cases}.
\end{align}
The second is the third-rank alternating tensor, also known as the Levi-Civita, which is defined as
\begin{align}
\epsilon_{ijk} = \begin{cases}
\quad 0& \text{if}~~i=j, j=k~~\text{or} ~~i=k,\\\
\quad 1& \text{for}~~(i,j,k)=(1,2,3), (2,3,1) ~~\text{or} ~~(3,1,2),\\
\quad -1& \text{for}~~(i,j,k)=(1,3,2), (2,1,3)~~ \text{or} ~~(3,2,1).\\
\end{cases}
\end{align}

{\bf Tensor algebra: } 
Using these rules, the notation for tensor algebra is as follows. Here $w$ is a scalar, $\ba$, $\bb$ and $\bc$ are vectors, $\bT$, $\bS$, $\bA$ and $\bB$ are second rank tensors.
\begin{center}
\begin{tabular}{lllc}
                           & {\bf Vector notation}         & {\bf Index notation}         & {\bf Tensorial rank} \\
 Sum of vectors:           & $\bc = \ba + \bb$             & $c_i = a_i + b_i$            &  1 \\
 Dot product of vectors:   & $w = \ba \cdot \bb$           & $w = a_i b_i$                &  0 \\
 Dot product of tensors:   & $\bT = \bA\cdot\bB$           & $T_{ij} = A_{ik}B_{kj}$      &  2 \\
 Outer product of vectors  & $\bA = \ba \bb$               & $A_{ij} = a_i b_j$           &  2 \\
 Outer product of tensors: & $\bA \bB$                     & $A_{ij} B_{km}$              &  4 \\
 Contraction:              & $\text{tr}(\bA)$              & $A_{ii}$                     &  0 \\
 Scalar triple product:    & $\ba\cdot(\bb \times \bc)$    & $\epsilon_{ijk} a_i b_j c_k$ &  0 \\
 Vector triple product:    & $\ba \times (\bb \times \bc)$ & $\epsilon_{ijk} \epsilon_{kmn} a_j b_m c_n$ & 1 
\end{tabular}
\end{center}
A combination of the above operations is a contraction between two second rank tensors denoted by $\bA : \bB$ defined as $A_{ik}B_{ki}$. Note that $\bA : \bB = \text{tr}(\bA \cdot \bB)$.

A symmetric second-rank tensor is on that satisfies $S_{ij}=S_{ji}$ for all combinations of $i$ and $j$. An anti-symmetric one is that satisfies $A_{ij} = -A_{ji}$.

Components of vectors transform in a predictable way when represented in a different coordinate system. In other words, if a vector $\bu$ has components $u_i$, i.e. $\bu = u_i \e_i$ (note: sum implied), then the components of $\bu$ change when the basis vectors change to $\e'_i$. Similarly, tensor components also change in a predictable way under coordinate transformations. This can be seen using the representation of tensors in terms of the Cartesian basis vectors as $\bT = T_{ij} \e_i \e_j$ (sum implied). Note that $\e_i \e_j$ is the outer product between $\e_i$ and $\e_j$, and just like $\e_i$ is a unit vector, the term $\e_i \e_j$ is one of the nine unit second-rank tensors. 

The reason to introduce the Kronecker delta and the alternating tensor are that they are special second and third rank tensors. They are the so-called ``unit isotropic'' tensors, i.e. the Cartesian components of these tensors do not change under coordinate transformations. (We shall take it for granted here without proof.) By extension, any algebraic expressions constructed using these unit isotropic tensors are also isotropic. In fact, the most general second, third, and fourth rank isotropic tensors are 
\begin{subequations}
\begin{align}
I^2_{ij}   &= \lambda \delta_{ij} \label{eqn:2ndiso} \\
I^3_{ijk}  &= \lambda \epsilon_{ijk} \label{eqn:3rdiso} \\
I^4_{ijkl} &= \lambda \delta_{ij} \delta_{kl} + \alpha \delta_{ik} \delta_{jl} + \beta \delta_{il} \delta_{jk}, \label{eqn:4thiso}
\end{align}
\end{subequations}
where $\lambda$, $\alpha$ and $\beta$ are arbitrary scalars.

The so-called $\epsilon-\delta$ identity is useful in simplifying the inner products of two alternating tensors as
\begin{align}
 \epsilon_{ijk} \epsilon_{kmn} = \delta_{im} \delta_{jn} - \delta_{in} \delta_{jm}.
\end{align}
Here note that the left hand side is an isotropic fourth-rank tensor, which is expressed on the right-hand side as a special case of the most general isotropic fourth-rank tensor.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tensor Calculus}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tensorial notation also aids in representing multivariate calculus of fields. Here are the basic elements in terms of scalar field $\phi(\bx, t)$, vector field $\bu(\bx, t)$ and tensor fields $\bT(\bx,t)$. A special notation called the comma notation is used the denote derivatives with respect to Cartesian coordinates, $\partial \phi/\partial x_i = \phi_{,i}$, i.e. an index following a comma in the subscript of a symbol implies differentiation with respect to the corresponding Cartesian coordinate.
\begin{center}
\begin{tabular}{lllc}
                      & {\bf Vector notation} & {\bf Index notation}                             & {\bf Tensorial rank} \\
Gradient of a scalar:   & $\grad \phi$          & $\dfrac{\partial \phi}{\partial x_i} = \phi_{,i}$ & 1 \\
Divergence of a vector: & $\grad \cdot \bu$     & $\dfrac{\partial u_i}{\partial x_i}  = u_{i,i}$     & 0 \\
Gradient of a vector:   & $\grad \bu$           & $\dfrac{\partial u_i}{\partial x_j}  = u_{i,j}$     & 2 \\ 
Divergence of a tensor: & $\grad \cdot \bT$     & $\dfrac{\partial T_{ij}}{\partial x_j} = T_{ij,j}$ & 1 \\
Curl of a vector:       & $\grad \times \bu$    & $\epsilon_{ijk} \dfrac{\partial u_k}{\partial x_j} = \epsilon_{ijk} u_{k,j}$ & 1 
\end{tabular}
\end{center}

Results in integral calculus may also be condensed using the index notation. Consider a volume $\Omega$ in space, with its unit normal oriented outwards denoted by $\nhat$. The divergence theorem expressed in vector and index notation is
\begin{align}
 \int_{\partial \Omega} \bu \cdot \nhat~dA = \int_{\Omega} \grad\cdot\bu~d\Omega \quad \text{or} \quad 
 \int_{\partial \Omega} u_i n_i~dA = \int_\Omega u_{i,i}~d\Omega, 
\end{align}
where $\partial \Omega$ represents the bounding surface of $\Omega$ and $dA$ is the infinitesimal area element on it. A generalization of this theorem for second-rank tensor is
\begin{align}
 \int_{\partial \Omega} \bT \cdot \nhat~dA = \int_{\Omega} \grad\cdot\bT~d\Omega \quad \text{or} \quad 
 \int_{\partial \Omega} T_{ij} n_j~dA = \int_\Omega T_{ij,j}~d\Omega.
\end{align}
Stokes theorem applies to closed curves $C$, the unit tangent to it $\that$, the surface it encloses $S$ and the unit normal to the surface $\nhat$.
For vectors, it reads
\begin{align}
\int_C \bu\cdot \that~ds = \int_S (\grad\times\bu)\cdot\nhat~dA \quad \text{or} \quad \int_C u_i t_i~ds = \int_S \epsilon_{ijk} u_{kj}n_i~dA,
\end{align}
where $ds$ is the infinitesimal arc-length element.
% It may also be generalized to tensors, but we do not need to in these notes.
