\chapter{Potential flow}
The case where the fluid velocity in given by
\begin{align}
 \bu = \grad \phi, \qquad \nabla^2 \phi = 0, \label{eqn:potflow}
\end{align}
is called potential flow.
Velocity profiles of this type arise in a number of situations in practice, but most importantly this theory allows fluid dynamicists to construct flow profiles that automatically satisfy conservation of mass and momentum. Here we treat this subject.

\section{Conditions for and implications of potential flow}
The conditions under which potential flow is realized are those of incompressibility and irrotationality, i.e.,
\begin{align}
 \grad\cdot\bu = 0 \qquad \text{and} \qquad \grad\times\bu = \boldsymbol{0}, \text{ respectively}.
\end{align}
A necessary and sufficient condition for irrotationality is
\begin{align}
 \grad\times\bu = 0 \qquad \Longleftrightarrow \qquad \bu = \grad \phi,
\end{align}
where $\phi$ is a scalar field known as the velocity potential. Substituting this form of $\bu$ into the incompressibility conditions yields Laplace equation $\nabla^2\phi = 0$ for $\phi$. In this way, mass is conserved.

We have seen in \eqref{eqn:Bernoulliunsteady}, that momentum is conserved if the pressure satisfies the Bernoulli equation, i.e.,
\begin{align}
 \rho \pd{\phi}{t} + \dfrac{1}{2} \rho |\bu|^2 + p - \rho \bg \cdot \bx = \text{constant in $\bx$}.
\end{align}

In this manner, potential flow satisfies mass and momentum conservation for a viscous fluid.

To construct a potential flow, one solves the Laplace equation \eqref{eqn:potflow} for $\phi$ subject to appropriate boundary conditions. Given the nature of Laplace equation, on the boundaries only one of the two components of velocity may be imposed, but not both. On the boundary between the fluid and a solid object, a condition of no-normal flow is imposed, i.e.,
\begin{align}
 \grad \phi \cdot \nhat = 0, \qquad \text{on solid walls.}
\end{align}
Therefore, in general, the no-slip condition cannot be satisfied by potential flow past solid objects and potential flow generally slips past the walls. Regardless, potential flow is incredibly succcessful in providing insight into situations where solid bodies are either not present or could be abstracted away.

\section{Incompressible and irrotational flow in two-dimensions}
While the general concept of potential flow applies in three dimensions, the application of complex variables makes potential flow much more powerful in two dimensions. Essentially, the use of complex variables trivializes the solution of Laplace equation. The only remaining complexity that remains is the satisfaction of the boundary conditions. In the rest of this chapter, we will focus on two-dimensional incompressible irrotational flow.

Irrotationality in two dimension implies for the components of velocity $\bu = (u,v)$
\begin{align}
 \grad \times \bu = \left( \pd{v}{x} - \pd{u}{y} \right) \e_z = \boldsymbol{0} \qquad \Longleftrightarrow \qquad u = \pd{\phi}{x},\quad v=\pd{\phi}{y}.
 \label{eqn:2dpotential}
\end{align}
Similarly, incompressibility implies
\begin{align}
 \grad \cdot \bu = \pd{u}{x} + \pd{v}{y} = 0 \qquad \Longleftrightarrow \qquad u = \pd{\psi}{y}, \quad v=-\pd{\psi}{x},
\label{eqn:2dstreamfunc}
\end{align}
where $\psi$ is a field called stream function. 

This type of reciprocal relation between the deriviatives of two scalar functions, in our case $\phi$ and $\psi$, are called the Cauchy-Reimann conditions:
\begin{align}
\pd{\phi}{x} = \pd{\psi}{y}, \qquad \pd{\phi}{y} = - \pd{\psi}{x}.
\end{align}
Owing to this relation, both $\phi$ and $\psi$ satisfy Laplace equation, i.e., $\nabla^2\phi = \nabla^2 \psi = 0$. Furthermore, $\phi$ and $\psi$ are the real and imaginary part of a holomorphic function $w$ of the complex variable $z$, i.e.
\begin{align}
 w(z) = \phi(x,y) + i\psi(x,y), \qquad \text{where} \qquad z = x+iy.
\end{align}
Thus, the strategy for generating potential flows is to construct holomorphic functions $w(z)$ called complex potentials that satisfy the necessary boundary condition. The fluid velocity is given by the relation
\begin{align}
 u - iv = \dfrac{dw}{dz}.
\end{align}


Here are some geometric relations between $\phi$, $\psi$ and streamlines.
\subsection{Stream function and streamlines}
Contours of constant stream function are streamlines of the flow. To see this, consider the directional derivative of the stream function in the direction of the streamline, i.e. in the direction of velocity
\begin{align}
 \grad\psi \cdot \bu = \pd{\psi}{x} u + \pd{\psi}{y}v = -vu + uv = 0.
\end{align}
Thus, irrespective of location in the flow, $\psi$ does not vary in the direction of streamlines. Therefore, $\psi$ is a constant along a streamline. In other words, streamlines are contours of constant stream function.

\subsection{Stream function and velocity potential}
Contours of constant stream function and those of constant velocity potential meet orthogonally. To see this, consider the dot product of $\grad \phi$ and $\grad \psi$, the normals of the contours at a given point:
\begin{align}
\grad \phi \cdot \grad \psi = \pd{\psi}{x} \pd{\phi}{x} + \pd{\psi}{y} \pd{\phi}{y} = 0.
\end{align}
Since the normals are orthogonal, the tangents must be too, and therefore the curves must be too.

\section{Elementary complex potentials}
Some basic elements that are used to construct the complex potentials.
\subsection{Uniform flow}
Consider the complex potential
\begin{align}
 w(z) = Ue^{-i\alpha} z \qquad \Longrightarrow \qquad \phi = U (x\cos\alpha + y \sin\alpha), \quad \psi = U(y\cos\alpha - x \sin\alpha).
\end{align}
The flow velocity corresponding to this potential is
\begin{align}
u = U \cos\alpha, \qquad v = U\sin \alpha,
\end{align}
i.e., a uniform flow at an angle $\alpha$ to the $x$-axis. An example of this flow is shown in Figure~\ref{fig:Elementary}(a).
\begin{figure}[ht]
\centerline{\includegraphics{Elementary}}
\caption{Visualization of Elementary flows. Streamlines are in blue and equipotential lines are in red. (a) Uniform flow at an angle of 30$^\circ$ to the $x$-axis. Potential $w(z) = ze^{i\pi/6}$. (b) Flow due to a point source. Potential $w(z)=\log z$. (c) Flow due to a point vortex. Potential $w(z) = i\log z$. (d) Flow due to a point dipole. Potential $w(z) = 1/z$. The black circle in panels (b) and (c) depicts the contour used for illustrating the concentrated nature of the point source and vortex.}
\label{fig:Elementary}
\end{figure}
\subsection{Point source}
Consider the complex potential
\begin{align}
 w(z) = \dfrac{Q}{2\pi} \log z \qquad \Longrightarrow \qquad \phi = \dfrac{Q}{2\pi} \log r, \quad \psi = \dfrac{Q\theta}{2\pi},
\end{align}
where $z = r^{i\theta}$ is the polar representation of the complex variable $z$.
The flow velocity corresponding to this potential is
\begin{align}
\bu = \dfrac{Q}{2\pi r} \e_r,
\end{align}
i.e., a radially outward flow decaying inversely proportional with distance. An example of this flow is shown in Figure~\ref{fig:Elementary}(b). For what follows, let $\bu\cdot\e_r = u_r$ and $\bu\cdot\e_\theta = u_\theta$.

To understand why the flow corresponding to this potential is termed as the point source, consider the divergence and curl of the velocity field:
\begin{align}
 \grad\cdot\bu = \dfrac{1}{r} \left( \pd{(ru_r)}{r} + \pd{u_\theta}{\theta} \right) = 0, \\
 \grad\times\bu = \dfrac{1}{r} \left( \pd{(ru_\theta)}{r} - \pd{u_r}{\theta} \right) \e_z = \boldsymbol{0} 
\end{align}
for $r>0$, but this calculation says nothing about $r=0$. To deduce that, consider the line integrals along the circle of radius $r$
\begin{align}
\int_C \bu\cdot\nhat~ds = \int_0^{2\pi} \dfrac{Q}{2\pi r} \e_r \cdot \e_r~rd\theta = Q, \label{eqn:sourcenml}\\
\int_C \bu\cdot\that~ds = \int_0^{2\pi} \dfrac{Q}{2\pi r} \e_r \cdot \e_\theta~rd\theta = 0, \label{eqn:sourcetgt} 
\end{align}
where $\nhat$ and $\that$ are the unit normal and tangential vectors to the curve $C$.
Given that, due to the divergence and Stokes theorems, respectively, 
\begin{align}
 \int_C \bu\cdot\nhat~ds = \int_A \grad\cdot \bu~dA, \quad \text{and} \label{eqn:divergence}\\
 \int_C \bu\cdot\nhat~ds = \int_A (\grad\times \cdot \bu)\cdot\e_z~dA, \label{eqn:stokes}
\end{align}
the inesacapable conclusion is that for this flow $\grad\cdot\bu = Q\delta(\bx)$, where $\delta$ is the Dirac-delta function and $\grad\times\bu = \boldsymbol{0}$. Thus, this flow is irrotational everywhere, but incompressible only for $r>0$. Volume is not conserved at $r=0$, but instead is generated there. Hence the terminology to describe this flow as a ``point source''.

\subsection{Point vortex}
Consider the complex potential
\begin{align}
 w(z) = -\dfrac{i\Gamma }{2\pi} \log z \qquad \Longrightarrow \qquad \phi = \dfrac{\Gamma \theta}{2\pi}, \quad \psi = -\dfrac{\Gamma} {2\pi} \log r.
\end{align}
The flow velocity corresponding to this potential is
\begin{align}
\bu = \dfrac{Q}{2\pi r} \e_\theta,
\end{align}
i.e., an azimuthal flow decaying inversely proportional with distance. An example of this flow is shown in Figure~\ref{fig:Elementary}(c).

We again examine $\grad\cdot\bu$ and $\grad\times\bu$ to understand the terminology associated with this flow.
Just as for the point source,
\begin{align}
 \grad\cdot\bu = \dfrac{1}{r} \left( \pd{(ru_r)}{r} + \pd{u_\theta}{\theta} \right) = 0, \\
 \grad\times\bu = \dfrac{1}{r} \left( \pd{(ru_\theta)}{r} - \pd{u_r}{\theta} \right) \e_z = \boldsymbol{0} 
\end{align}
for $r>0$, but which says nothing about $r=0$. The line integrals along the circle of radius $r$
\begin{align}
\int_C \bu\cdot\nhat~ds = \int_0^{2\pi} \dfrac{\Gamma}{2\pi r} \e_\theta \cdot \e_r~rd\theta = 0, \label{eqn:vortexnml}\\
\int_C \bu\cdot\that~ds = \int_0^{2\pi} \dfrac{\Gamma}{2\pi r} \e_\theta \cdot \e_\theta~rd\theta = \Gamma, \label{eqn:vortextgt} 
\end{align}
which, in view of (\ref{eqn:divergence}-\ref{eqn:stokes}) lead to the inescable conclusion that the flow is incompressible everywhere but irrotational only for $r>0$. The vorticity is, in fact, $\grad\times\bu = \Gamma \delta(\bx) \e_z$. Hence the flow due to this potential is termed as the ``point vortex''.

\subsection{Point dipole}
The flow constructed by the linear superposition of a point source and an equal point sink (i.e., a source with negative strength) located vanishingly small distances apart constitutes a point dipole.
The complex potential is constructed as
\begin{align}
 w(z) = \lim_{\epsilon\to 0} \dfrac{Q}{2\pi} \left( \log (z+\epsilon) - \log(z-\epsilon) \right).
\end{align}
Of course, in the limit $\epsilon \to 0$, the potential would vanish, unless the source-sink pair also strengthened in the process. In particular, let $Q = D/(2\epsilon)$, so that the source-sink pair strength is inversely proportional to the distance between them. Here $D$ is the strength of the point dipole. With this assumption,
\begin{align}
 w(z) = \lim_{\epsilon\to 0} \dfrac{D}{2\pi} \left( \dfrac{\log (z+\epsilon) - \log(z-\epsilon)}{2\epsilon} \right) = \dfrac{D}{2\pi z}. 
\end{align}
The flow due to the point dipole is depicted in Figure~\ref{fig:Elementary}(d). The streamlines and equipotential lines are both sets of circles that are tangential to the $x$ and $y$ axes, respectively, at the origin. 

\subsection{Higher multipoles}
Note that, due to its construction, the potential due to a point dipole is the derivative of the potential due to a point source. This can be extended to higher orders called ``multipoles''. For example, a point quadrupole is a point dipole of point dipoles with a complex potential proportional to $1/z^2$. And octupole is a point dipole of quadrupoles with potential $1/z^3$. And so on.

In this manner, the general complex potential with the farfield approaching uniform flow may be written as
\begin{align}
 w(z) = Uz + \dfrac{Q-i\Gamma}{2\pi} \log z + \dfrac{a_1}{z} + \dfrac{a_2}{z^2} + \dfrac{a_3}{z^3} + \dots.
 \label{eqn:seriespotential}
\end{align}
Here, the coefficients $U$, $Q$, $\Gamma$, $a_1$, $a_2$, etc, may be functions of time.

\section{Potential flow past immersed bodies}
The coefficients $Q$, $\Gamma$, $a_1$, $a_2$, \dots, may be chosen to construct the complex potential for flow past bodies. Here are some examples.

\subsection{Rankine half-body}
The linear superposition of a uniform flow and a point source may be used to represent flow past a semi-infinite body, called the Rankine half-body. The complex potential is
\begin{align}
w(z) = Uz + \dfrac{Q}{2\pi} \log z.
\label{eqn:Rankine}
\end{align}
The flow streamlines and equipotential lines are shown in Figure~\ref{fig:Rankine}(a).
\begin{figure}[ht]
\centerline{\includegraphics{Rankine}}
\caption{Flow around a Rankine half-body. (a) The flow streamlines (blue) and equipotential lines (red) for the potential given by \eqref{eqn:Rankine} for $U=1$ and $Q=\pi$. (b) Same as (a) but with the streamline $\psi=Q/2$ drawn in solid black and the area inside this streamline shaded in blue. The shaded region is the Rankine half-body.}
\label{fig:Rankine}
\end{figure}
Any region of the plane on one side of a streamline may be replaced by a solid body and the complex potential in the remaining region describs potential flow past this solid body. For example, consider the streamline given by $\psi=Q/2$, where
\begin{align}
\psi = \text{Im}(w) = U r \sin\theta + \dfrac{Q\theta}{2\pi} \quad \text{so the streamline is} \quad r = \dfrac{Q(\pi-\theta)}{2\pi U \sin\theta}.
\end{align}
The streamline is shown in Figure~\ref{fig:Rankine}(b) and the area inside the streamline, shaded blue in the figure, is the Rankine half-body.
The complex potential $w(z)$ in \eqref{eqn:Rankine} describes the flow past this Rankine half-body.

\subsection{Rankine oval}
The linear superposition of a uniform flow, a point source and a point sink may be used to represent flow past a finite body, called the Rankine oval. The complex potential is
\begin{align}
w(z) = Uz + \dfrac{Q}{2\pi} \left(\log (z-1) - \log(z+1)\right).
\label{eqn:RankineOval}
\end{align}
The flow streamlines and equipotential lines are shown in Figure~\ref{fig:RankineOval}(a).
\begin{figure}[ht]
\centerline{\includegraphics{RankineOval}}
\caption{Flow around a Rankine oval. (a) The flow streamlines (blue) and equipotential lines (red) for the potential given by \eqref{eqn:RankineOval} for $U=1$ and $Q=\pi$. (b) Same as (a) but with the streamline $\psi=0$ drawn in solid black and the area inside this streamline shaded in blue. The shaded region is the Rankine half-body.}
\label{fig:RankineOval}
\end{figure}
Consider the streamline given by $\psi=0$, where
\begin{align}
\psi = \text{Im}(w) = U r \sin\theta + \dfrac{Q(\theta_1-\theta_2)}{2\pi} \quad \text{so the streamline is given by} \quad Ur\sin\theta + \dfrac{Q(\theta_1-\theta_2)}{2\pi} = 0,
\end{align}
where $\tan \theta_1 = r\sin\theta/(r\cos\theta+1)$ and $\tan \theta_2 = r\sin\theta/(r\cos\theta-1)$.
The streamline is shown in Figure~\ref{fig:RankineOval}(b) and the area inside the streamline, shaded blue in the figure, is the Rankine oval.
The complex potential $w(z)$ in \eqref{eqn:RankineOval} describes the flow past this Rankine oval.

\subsection{Circle}
The Rankine oval approaches a circle as the source and the sink approach each other to form a dipole. The complex potential in this case is
\begin{align}
w(z) = U \left(z + \dfrac{a^2}{z}\right).
\label{eqn:Circle}
\end{align}
The flow streamlines and equipotential lines are shown in Figure~\ref{fig:Circle}(a).
\begin{figure}[t]
\centerline{\includegraphics{Circle}}
\caption{Flow around a Rankine oval. (a) The flow streamlines (blue) and equipotential lines (red) for the potential given by \eqref{eqn:Circle} for $U=1$ and $a=1$. (b) Same as (a) but with the streamline $\psi=0$ drawn in solid black and the area inside this streamline shaded in blue. The shaded region is a circle. (c) The flow around a spinning circle corresponding to the complex potential given by \eqref{eqn:Circlefull} for $U=1$, $a=1$, $\Gamma=2\pi$.}
\label{fig:Circle}
\end{figure}
Consider the streamline given by $\psi=0$, where
\begin{align}
\psi = \text{Im}(w) = U\sin\theta \left( r - \dfrac{a^2}{r} \right), \quad \text{ so that $\psi=0$ on} \quad r=a.
\end{align}
The streamline is shown in Figure~\ref{fig:Circle}(b) and the area inside the streamline, shaded blue in the figure, is a circle of radius $a$.
The complex potential $w(z)$ in \eqref{eqn:Circle} describes the flow past this circle.

An arbitrary point vortex centered at the center of the circle may be added to the mix without violating the boundary condition on the circle. The most general potential flow around a circle is given by
\begin{align}
 w(z) = U \left(z + \dfrac{a^2}{z}\right) - \dfrac{i\Gamma}{2\pi} \log z.
 \label{eqn:Circlefull}
\end{align}
Here $\Gamma$ is the circulation around the circle, perhaps due to the spinning of the circle.
The flow due to this potential is shown in Figure~\ref{fig:Circle}(c).

\section{Force on a immersed body}
The force on the body due to the potential flow \eqref{eqn:seriespotential} may be calculated by integrating the force due to the fluid pressure. 
The force on the body per unit length out of the plane of the paper is
\begin{align}
 \bF = \int_C -p \nhat~ds, 
\end{align}
where $C$ is the bounding closed streamline defining the body. 
The fluid pressure is determined by Bernoulli equations as
\begin{align}
 p = -\rho\left( \pd{\phi}{t} + \dfrac{1}{2} |\bu|^2 \right) + \rho \bg\cdot\bx,
\end{align}
The contribution to the force due to gravity is the force of buoyancy from Archimedes principle. Here we calculate the contribution from the remaining two terms.

\subsection{Force in a steady potential flow}
In steady potential flow, only the term $\rho |\bu|^2/2$ contributes to the net unbalanced force beyond buoyancy.
In complex variables, let the components of the force $\bF = F_x\e_x + F_y \e_y$ be written as $F_x + iF_y$. If the tangent to $C$ makes an angle $\theta$ to the $x$-axis, then $\bu = |\bu|e^{i\theta}$ because the flow is tangential to the streamline. Also, because the components of velocity are $w'(z) = u-iv = |\bu| e^{-i\theta}$, $|\bu|^2 = w'(z)^2 e^{2i\theta}$. The product of the unit normal and the infinitesimal line element is $-ie^{i\theta}~ds$. The complex conjugate of the force is
\begin{align}
F_x - iF_y = \int_C \dfrac{1}{2} \rho w'(z)^2 e^{2i\theta}~(ie^{-i\theta}~ds) = \dfrac{i\rho}{2} \int_C w'(z)^2~dz,
\label{eqn:steadyforce}
\end{align}
where the complex integration infinitesimal $dz=e^{i\theta}~ds$.

Using this expression for force, and substituting \eqref{eqn:seriespotential} in \eqref{eqn:steadyforce} yields
\begin{align}
 F_x - i F_y = -\rho U (Q - i\Gamma),
\end{align}
where Cauchy's residue theorem is employed for calculating the contour integral. Law of conservation of mass demands that for a body immersed in the flow $Q=0$, and therefore, 
\begin{align}
 F_x = 0 \qquad \text{and} \qquad F_y = -\rho U \Gamma.
 \label{eqn:dalembertkuttajoukowsky}
\end{align}

The drag on an object is the force it experiences in the direction of the flow, while the lift is the force perpendicular to the flow.
According to \eqref{eqn:dalembertkuttajoukowsky}, the lift on an immersed body due to the potential flow is $\rho U \Gamma$, which is the statement of the Kutta-Joukowsky (also spelled Joukowski, Zhukowski or Zhukovsky) theorem in aerodynamics. The lift on the spinning circle, due to this theorem, is $\rho U \Gamma$. 

The fact that a body immersed in a potential flow does not experience any drag is the subject of the D'Alembert paradox. Neither the Rankine oval, nor the circle, nor body of any other shape experiences any drag in potential flow. D'Alembert paradox exist because potential flow does not satisfy the no-slip condition. The friction of a viscous fluid with the solid boundary generates vorticity, which is transported by the fluid to regions away from the immersed body. In this manner, application of the no-slip boundary condition on viscous fluids invalidates the assumptions underlying potential flow, especially that the flow be irrotational. In fact, the shedding of vorticity into the flow is the origin of fluid dynamical drag (this we state without proof at this point).

\subsection{Added mass from the unsteady term}
Consider the contribution of the term $\rho \pd{\phi}{t}$. The resulting force is
\begin{align}
 \int_C \rho \pd{\phi}{t}\nhat ~ds.
\end{align}
Let us examine this expression for the case of flow around a circle, so that $w(z)$ is given by \eqref{eqn:Circle}. This corresponds to 
\begin{align}
 \phi = \text{Re}(w) = U \cos\theta \left(r + \dfrac{a^2}{r} \right), \qquad \text{which on $r=a$ is} \qquad \phi = 2Ua\cos\theta. 
\end{align}
The integral for force yields
\begin{align}
 \bF = \int_0^{2\pi} 2 \rho \dfrac{dU}{dt} a \cos\theta (\cos\theta \e_x + \sin\theta \e_y) a~d\theta = \rho \pi a^2 \dfrac{dU}{dt} \e_x. 
\end{align}

This result may be interpreted in the following way. Any acceleration of the circle implies an acceleration of the fluid around it. This acceleration needs an additional force proportional to the acceleration. The coeffient of proportionality between this additional force and the acceleration is the ``added mass'' the fluid presents to the motion of the circle. In this case, the added mass in $\rho \pi a^2$. 

\section{Flow around an airfoil}
The lift generated by an airfoil is a central result in aerodynamics, which makes use of potential flow. 
\begin{figure}
\centerline{\includegraphics[width=0.7\textwidth]{AirfoilStreamlines}}
\caption{Flow around an airfoil at an angle of attack computed using the commercial software COMSOL. The streamlines are shown in blue, and the black curve is the stagnation streamline, part of which makes the airfoil shape. The angle of attack for this case is 14$^\circ$.}
\label{fig:AirfoilStreamlines}
\end{figure}
Some salient features of this flow are visualized in Figure~\ref{fig:AirfoilStreamlines}. The stagnation streamline is incident on the lower surface of the airfoil, which develops the stagnation pressure under the airfoil. The streamlines curve around the leading edge of the airfoil, generating a low-pressure area above the airfoil. Finally and crucially, note that the stagnation streamline separates from the plate at the trailing edge. These are the features that will guide us in constructing the appropriate potential flow.

Here we will present one calculation, that of the lift on the prototypical airfoil -- a flat plate, which hightlights this central result. The underlying potential flow is constructed in this case using a technique known as conformal mapping (the same technique can be used to construct flow around airfoils of other shapes). 

A conformal map is a map between two complex variables, which we take to be $z$ and $\zeta$. Here $z=x+iy$ corresponds to the physical spatial variable, in which the flat plate exists, and $\zeta$ is the mapped variable, in which the flat plate is mapped to a circle. The particular map we use is called the Joukowsky map:
\begin{align}
 z = f(\zeta) = (1+\epsilon) \zeta + (1-\epsilon)\dfrac{a^2}{\zeta}.
\end{align}
A circle in $\zeta$-plane, $\zeta = a e^{i\theta}$ is mapped to 
\begin{align}
 z = a (1+\epsilon)e^{i\theta} + a(1-\epsilon) e^{-i\theta} = 2a \cos\theta + 2i a\epsilon \sin\theta,
\end{align}
which is an ellipse of major axis $2a$ along the $x$-axis and minor axis $2a\epsilon$ along the $y$-axis. In the limit $\epsilon \to 0$, the ellipse approaches a flat plate $-2a\le x \le 2a$. The point $\zeta=a$ is mapped to the trailing edge of the plate $z=2a$. The chord of the flat plate is $c=4a$.

While it is possible to solve for the inverse map $\zeta = f^{-1}(z)$ in closed form, the idea is to not need the explicit expression for the inverse. However, we do need the inverse map to be analytic so that the complex potential
\begin{align}
W(z) = w\left(f^{-1}(z)\right)
\end{align}
automatically deforms the streamlines and converts the circular streamine in the $\zeta$ coordinates to be a flat plate in the $z$ coordinates, whilst preserving the analyticity of the complex potential.

\begin{figure}[ht]
\includegraphics{FlatPlate1}
\caption{Flow around a flat plate at an angle of 30$^\circ$ to the flow and $\Gamma=0$. (a) The flow in the $\zeta=\xi + i\eta$ coordinates. Streamlines are in blue and equipotential lines in red. Black curve shows the stagnation streamline. (b) The same flow in the $z=x+iy$ coordinates. }
\label{fig:FlatPlate1}
\end{figure}

We start with the complex potential \eqref{eqn:Circlefull} for flow around a circle, but now expressed in $\zeta$ and with an angle of attack $\alpha$, i.e.,
\begin{align}
 w(\zeta) = U\left( \zeta e^{-i\alpha} + \dfrac{a^2}{\zeta e^{-i\alpha}} \right) - \dfrac{i\Gamma}{2\pi} \log \zeta.
\end{align}
As $\zeta \to \infty$, the largest term in $w(\zeta)$ is $U\zeta e^{-i\alpha}$, corresponding with uniform flow at an angle $\alpha$ to the flat plate.
The complex velocity in the $\zeta$ plane is
\begin{align}
 w'(\zeta) = U\left( e^{-i\alpha} - \dfrac{a^2}{\zeta^2} e^{i\alpha} \right) - \dfrac{i\Gamma}{2\pi \zeta}.
 \label{eqn:circlevelocity}
\end{align}

The velocity in the $z$-coordinates is
\begin{align}
\dfrac{dW}{dz} = \dfrac{w'(\zeta)}{z'(\zeta)} = \dfrac{U\left( e^{-i\alpha} - \dfrac{a^2}{\zeta^2} e^{i\alpha} \right) - \dfrac{i\Gamma}{2\pi \zeta}} {1-\dfrac{a^2}{\zeta^2}}.
\end{align}
As $\zeta \to \infty$, $dW/dz$ approaches $U$.

Consider the case $\Gamma=0$. An example of the flow corresponding to this case is shown in Figure~\ref{fig:FlatPlate1}. Note that the stagnation point in the $\zeta$-plane is at $\zeta = \pm ae^{i\alpha}$. This point maps in the $z$-plane to $z=- 2a \cos\alpha$ below the flat plate, and $z=2a\cos\alpha$ above it. Note that in this flow, the stagnation streamline does not separate from the trailing edge $z=2a$. 

The stagnation point may be moved by judiciously choosing the value of $\Gamma$. The condition that the trailing edge be the rear stagnation point is called the {\em Kutta condition}. We impose the Kutta condition by ensuring that the stagnation point in the $\zeta$-coordinates is at $\zeta=a$. Substituting the velocity $w'(\zeta) = 0$ at $\zeta=a$ yields
\begin{align}
 \Gamma = -4 \pi Ua \sin\alpha
 \label{eqn:Kutta}
\end{align}
The streamlines and equipotential lines corresponding to the value of $\Gamma$ that enforces the Kutta condition, shown in Figure~\ref{fig:FlatPlate2}, illustrate this condition.

\begin{figure}[ht]
\includegraphics{FlatPlate2}
\caption{Flow around a flat plate at an angle of 30$^\circ$ to the flow and $\Gamma$ given by \eqref{eqn:Kutta}. (a) The flow in the $\zeta=\xi + i\eta$ coordinates. Streamlines are in blue and equipotential lines in red. Black curve shows the stagnation streamline. (b) The same flow in the $z=x+iy$ coordinates. }
\label{fig:FlatPlate2}
\end{figure}

The lift $F_y$ on the falt-plate is given by 
\begin{align}
 F_y = - \rho U  \Gamma = 4 \pi \rho U^2 a \sin \alpha. 
 \label{eqn:LiftDim}
\end{align}
This result can be converted to a dimensionless form in terms of the coefficient of lift, $C_L$, defined as
\begin{align}
 C_L = \dfrac{F_y}{\dfrac{1}{2} \rho U^2 c}.
\end{align}
Using the result \eqref{eqn:LiftDim}, the lift coefficient depends on the angle of attack as
\begin{align}
 C_L = \dfrac{4 \pi \rho U^2 a \sin \alpha}{\dfrac{1}{2} \rho U^2 (4a)} = 2 \pi \sin\alpha.
 \label{eqn:LiftNonDim}
\end{align}
Figure~\ref{fig:LiftCoefficient} shows that the results of \eqref{eqn:LiftNonDim} compare well with the lift coefficient computed using the commercial software COMSOL for small $\alpha$. The agreement worsens around $\alpha \approx 10^\circ$ due to a phenomenon called stall, in which the stagnation streamline also separates from the leading edge. A careful treatment of the influence of viscosity is needed to gain a deeper understanding of stall.

\begin{figure}[ht]
\centerline{\includegraphics{LiftCoefficient}}
\caption{Lift coefficient $C_L$ as a function of angle of attack $\alpha$. Blue circles show the lift calculated from computations in COMSOL. Black line shows results of \eqref{eqn:LiftNonDim}.}
\label{fig:LiftCoefficient}
\end{figure}
