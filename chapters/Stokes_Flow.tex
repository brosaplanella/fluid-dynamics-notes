\chapter{Stokes Flow}\label{ch:stokesflow}
In this chapter, we will be considering situations where inertial forces are negligible such that the flows have small Reynolds numbers. Such flows are named Stokes flows after George G. Stokes who first calculated the drag on a sphere moving through a viscous fluid. The applications of Stokes flows are wide ranging, from biological applications at the small scale (e.g. the swimming of microorganisms) to geological applications at the large scale (e.g. the flow of lava).

\section{Recap: Governing Equations}

\subsection{Navier-Stokes Equations}

For an incompressible fluid, the equations for mass and momentum balance are (see \S \ref{s:massbalance} and \S\ref{s:momentumbalance}):
\begin{align}
\grad\cdot \bu =0 \quad \text{and} \quad
\rho \dfrac{D\bu}{Dt} = {\bf f}+ \grad\cdot\bT,
\label{mom_bal}
\end{align}
where $\bu$ is the velocity, $\rho$ is the constant density, $t$ is time,  $\bT$ is the stress tensor and $\bf f$ is a body force (i.e. $\bf f=\rho \bg$).

Internal viscous stresses are produced by deformation. The fluid is Newtonian if the relationship between the rate of deformation ($\gamma_{ij}=\partial u_i/\partial x_j$) and the stress $\bT$ is local, linear, instantaneous and isotropic. If the fluid is also incompressible, the Newtonian constitutive law is
\begin{align}
\bT=-p\bI+\mu \left(\grad \bu+\grad \bu^T\right).
\end{align}
Substituting this into the momentum balance (\ref{mom_bal}) gives the {\it incompressible Navier-Stokes equations:}
\begin{align}
\rho \dfrac{D\bu}{Dt} = -\grad p +\mu\grad^2\bu +{\bf f}
\label{nav_sto}
\end{align}

\subsection{Reynolds number}

To non-dimensionalise the problem we write
\begin{align}
\bu=U\tilde{\bu}, \quad \bx = L\tilde{\bx}, \quad t = \frac{L}{U}\tilde{t}, \quad
p=P\tilde{p},
\end{align}
where $U$ is a characteristic velocity scale, $L$ is a characteristic lengthscale, and $P$ is a characteristic pressure scale. Substituting these into equation (\ref{nav_sto}) when $\bf f=\bf 0$ and rearranging we arrive at
\begin{align}
\frac{\rho UL}{\mu} \left(\dfrac{\partial\tilde{\bu}}{\partial\tilde{t}}
+\tilde{u}\cdot\tilde{\grad}\tilde{u}\right)
 = -\frac{PL}{\mu U}\tilde{\grad} \tilde{p} +\tilde{\grad}^2\tilde{\bu},
 \label{scale}
\end{align}
where $\tilde{\grad}\equiv\partial/\partial \tilde{x}_i$. We choose the viscous scaling for the pressure $P=\mu U/L$ (we could have chosen the alternative inertial scaling $P=\rho U^2$). This leaves one non-dimensional group called the Reynolds number:
\begin{align}
{\rm Re}=\frac{\rho U L}{\mu}=\frac{UL}{\nu}\equiv 
\frac{\rm inertia}{\rm viscous\, stresses},
%{\rm Re} \left(\dfrac{\partial\tilde{\bu}}{\partial\tilde{t}}
%+\tilde{u}\cdot\tilde{\grad}\tilde{u}\right)
% = -\tilde{\grad} \tilde{p} +\tilde{\grad}^2\tilde{\bu}.
 \label{Rey}
\end{align}
where $\nu=\mu/\rho$ is the kinematic viscosity ($\mu$ is denoted the dynamic viscosity).
In this section we will be focusing on the limit where ${\rm Re}\ll 1$ such that inertia is negligible and we can ignore the left-hand side of (\ref{scale}). The mass and momentum balance, known as {\it Stokes Equations}, are then given by
\begin{align}
\grad\cdot \bu =0 \quad \text{and} \quad {\bf 0} = -\grad p+\mu\grad^2 \bu +{\bf f}.
\end{align}

\subsection{Energy and Dissipation}
Taking the dot product of the full momentum equation (\ref{mom_bal}) with the velocity $\bu$ and integrating over a volume $\Omega$ gives
\begin{align}
\int_{\Omega} \rho \bu \cdot\frac{\partial \bu}{\partial t}\, \mathrm{d}\Omega
+\int_{\Omega} \rho \bu \cdot(\bu \cdot\grad \bu)\, \mathrm{d}\Omega
=\int_{\Omega}\bu\cdot {\bf f}\, \mathrm{d}\Omega
+\int_{\Omega}\bu \cdot (\grad \cdot {\bf T})\, \mathrm{d}\Omega.
\end{align}
This can also be written in summation convention
\begin{align}
\int_{\Omega} \rho u_i\frac{\partial u_i}{\partial t}\, \mathrm{d}\Omega
+\int_{\Omega} \rho u_i u_j\frac{\partial u_i}{\partial x_j}\, \mathrm{d}\Omega
=\int_{\Omega}u_i f_i\, \mathrm{d}\Omega
+\int_{\Omega}u_i\frac{\partial T_{ij}}{\partial x_j}\, \mathrm{d}\Omega.
\end{align}
Rearranging, applying the divergence theorem and mass conservation, the left-hand side becomes
\begin{align}
\mathrm{LHS}& = \int_{\Omega}\tfrac{1}{2}\frac{\partial}{\partial t}( \rho u_iu_i)-\tfrac{1}{2}u_iu_i\frac{\partial \rho}{\partial t}\, \mathrm{d}\Omega 
+\int_{\Omega}\tfrac{1}{2}\frac{\partial }{\partial x_j}\left(\rho u_iu_iu_j\right)
-\tfrac{1}{2}u_iu_i\frac{\partial}{\partial x_j}\left(\rho u_j\right)\, \mathrm{d}\Omega \nonumber\\
\nonumber
& = \frac{d}{dt}\int_{\Omega} \tfrac{1}{2}\rho u_iu_i \, \mathrm{d}\Omega
+\int_{\partial \Omega} \tfrac{1}{2}\rho u_iu_iu_jn_j \, \mathrm{d}A
-\int_{\Omega}\tfrac{1}{2}u_iu_i
\underbrace{\left(\frac{\partial \rho}{\partial t}+\frac{\partial}{\partial x_j}\left(\rho u_j\right)\right)}_{=0}
\, \mathrm{d}\Omega.
\end{align}
Similarly, the right-hand side becomes
\begin{align}
\mathrm{RHS}& =\int_{\Omega}u_i f_i\, \mathrm{d}\Omega
+\int_{\Omega}\frac{\partial}{\partial x_j}(u_iT_{ij})-T_{ij}\frac{\partial u_i}{\partial x_j}\, \mathrm{d}\Omega \nonumber\\
& = \int_{\Omega}u_i f_i\, \mathrm{d}\Omega+
\int_{\partial \Omega}u_iT_{ij}n_j \, \mathrm{d}A-
\int_{\Omega}T_{ij}\frac{\partial u_i}{\partial x_j}\, \mathrm{d}\Omega. \nonumber
\end{align}
The {\it mechanical energy equation} is then written as
\begin{align}
\underbrace{\frac{d}{dt}\int_{\Omega} \tfrac{1}{2}\rho u^2 \, \mathrm{d}\Omega}_{(1)}
+\underbrace{\int_{\partial \Omega} \tfrac{1}{2}\rho u^2\bu\cdot {\bf n} \, \mathrm{d}A}_{(2)}
=\underbrace{\int_{\Omega}\bu\cdot  {\bf f}\, \mathrm{d}\Omega}_{(3)}+
\underbrace{\int_{\partial \Omega}u_iT_{ij}n_j \, \mathrm{d}A}_{(4)}-
\underbrace{\int_{\Omega}T_{ij}\frac{\partial u_i}{\partial x_j}\, \mathrm{d}\Omega}_{(5)},
\label{mech}
\end{align}
where 
\begin{enumerate}
\item the rate of change of {\bf kinetic energy} in volume $\Omega$ is due to:
\item the flux of {\bf kinetic energy} over the boundary $A$,
\item the rate of working by body forces $\bf f$ in volume $\Omega$,
\item the rate of working of surface forces in $A$,
\item and the stress working in volume $\Omega$.
\end{enumerate}

The mechanical energy equation above is for a general continuum. We can now look more specifically for a Newtonian viscous fluid with constitutive law,
\begin{align}
T_{ij}=-p\delta_{ij}+2\mu e_{ij} \quad \mathrm{where}\quad
e_{ij}=\tfrac{1}{2}\left(\frac{\partial u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i}\right).
\end{align}
The stress working per unit volume then becomes
\begin{align}
T_{ij}\frac{\partial u_i}{\partial x_j}=-p\delta_{ij}\frac{\partial u_i}{\partial x_j}
+2\mu e_{ij}\frac{\partial u_i}{\partial x_j}.
\end{align}
Remembering some properties of the rate of strain and rotation tensors, we know that
\begin{align}
\delta_{ij}\frac{\partial u_i}{\partial x_j}=\frac{\partial u_i}{\partial x_i}=\grad\cdot\bu=0,
\quad
e_{ij}+r_{ij}=\tfrac{1}{2}\left(\frac{\partial u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i}\right)+\tfrac{1}{2}\left(\frac{\partial u_i}{\partial x_j}-\frac{\partial u_j}{\partial x_i}\right)=\frac{\partial u_i}{\partial x_j},
\end{align}
\begin{align}
e_{ij}r_{ij}=\tfrac{1}{4}\left(\frac{\partial u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i}\right)\left(\frac{\partial u_i}{\partial x_j}-\frac{\partial u_j}{\partial x_i}\right)=0.
\end{align}
Hence, the stress working, also known as the {\it viscous dissipation} $\Phi$, per unit volume is given by
\begin{align}
\Phi=T_{ij}\frac{\partial u_i}{\partial x_j}=2\mu e_{ij}e_{ij}.
\end{align}
The viscous dissipation $\Phi$ describes the rate of loss of mechanical energy due to friction in the volume $\Omega$.


\subsection{Additional comments}
The approximation of Stokes Flow is good if the velocity or length scales $U, \, L$ are small (e.g. swimming microorganisms), or the dynamic viscosity $\mu$ is large (e.g. in geophysics, flow in porous media, or the flow of lava or ice). Some examples are:
\begin{center}
\begin{tabular}{lllllll}
                           & {$L$}         & {$U$}         & {$\rho$} & {$\mu$} & {$\nu$} & {$\rm Re$} \\
 Sperm in water:	&$50\,\mu m$	& $200\,\mu m/s$	& $10^3\, kg /m^3$
 	& $10^{-3}\, Pa s$	& $10^6 \,m^2/s$ & $\bf 10^{-2}$\\
Mantle: & $3\times 10^3\, km$ & $1\, cm/yr$ & $3300\, kg/m^3$ & $10^{21}\,Pa s$ 
 &$3\times 10^{17}\,m^2/s$&$\bf 3\times 10^{-21}$\\
\end{tabular}
\end{center}
(See E. M. Purcell (1977) Life at Low Reynolds Number, American Journal of Physics).

\begin{figure}[ht!]
\centering
\includegraphics[width=.7\linewidth]{lava.png}
\caption{(a) Swimming sperm. (b) Flow of lava.}
\end{figure}

When approximating the Reynolds number, there are some caveats. Firstly there may be more than one relevant lengthscale, e.g in Lubrication theory later in the course the vertical lengthscale $H$ is much smaller than the horizontal lengthscale $L$. These lengthscales may also vary with position e.g. flow past a body where in the far field ${\rm Re} \sim U r/\nu$ where $r$ is the radial distance away from the body. Finally, the time scale may not be $L/U$ but instead may be specified by an external driving force e.g. an oscillating boundary condition with timescale $T\sim \omega ^{-1}$.

\section{Properties of Stokes Flows}

\begin{enumerate}
\item {\bf Instantaneity}
Instantaneity of the flow means there are no time derivatives $\partial \bu/\partial t$, so no inertia. The flow has no memory and only knows about the current boundary conditions and applied forcing, responding immediately to any changes. The flow is said to be ``quasi-steady'' as $\bu$ can vary in time if the boundary conditions and applied forcing ${\bf f}(t)$ varies in time.

E.g. for a translating body with speed ${\bf U}(t)$, the fluid force on the body is proportional to its velocity ${\bf f}(t)\propto {\bf U}(t)$ and not to its acceleration $d{\bf U}(t)/dt$.

\item {\bf Linearity:}
Linearity means that the flow is linearly forced whether it is by a boundary motion or body force. In Stokes Equations there is no $\bu\cdot \grad \bu$, so $\bu(\bx,t)$ and $p$ are linear in ${\bf f}$ and the boundary conditions. 


\begin{figure}[ht!]
\centering
\includegraphics[width=.25\linewidth]{translating_body.png}
\caption{A translating body.}
\end{figure}

\item {\bf Reversiblity:}
\noindent {\it Reversible in time:} if you apply a force ${\bf f}(t)$ in $0\leq t< T$ and then reverse the force and its history ${\bf f}(t)={\bf f}(2T-t)$ in $T\leq t<2T$ then the flow reverses and the fluid particles return to their starting position.

\noindent {\it Reversible in space:} reversibility in time and the symmetry of geometry can often be used to rule out some components of velocity $\bu$.

E.g. does a sphere sedimenting next to a vertical wall migrate towards the wall?
\begin{figure}[ht!]
\centering
\includegraphics[width=.5\linewidth]{sphere_wall.png}
\caption{Sedimenting sphere. Reversing the forcing $\bg$ must reverse the velocity $\bu$ by reversibility in time. Because of the symmetry of the sedimenting sphere we can reverse the system in $z$. In order for (a) to be the same as (c) we require the sphere to fall vertically with no horizontal component.}
\end{figure}

\item {\bf Forces Balance:}

Since there is no inertia the forces in the system must balance:
\begin{align}
\grad\cdot\bT = - {\bf f} \quad \Rightarrow \quad \int_{\Omega} \grad\cdot\bT\, \mathrm{d}\Omega
=\int_{\Omega} - {\bf f} \, \mathrm{d}\Omega.
\end{align}
Applying the divergence theorem then gives
\begin{align}
\int_{\partial \Omega} \bT\cdot {\bf n}\, \mathrm{d}A
+\int_{\Omega} {\bf f} \, \mathrm{d}\Omega=0.
\end{align}
This checks the consistency of the stress boundary conditions. In a similar way, we can check the consistency of the velocity boundary conditions by intergrating the mass balance equation:
\begin{align}
\grad\cdot \bu=0 \quad \Rightarrow \quad 
\int_{\Omega} \grad\cdot \bu \, \mathrm{d}\Omega = \int_{\partial \Omega} \bu\cdot {\bf n}\, 
\mathrm{d}A = 0.
\end{align}

\item {\bf Work Balances Dissipation:}
Since there is no kinetic energy in the mechanical energy balance, from (\ref{mech}) we know that the work done by body and surface forces must balance the viscous dissipation in a given volume:
\begin{align}
\int_{\Omega}\Phi \, \mathrm{d}\Omega
=\int_{\Omega}T_{ij}\frac{\partial u_i}{\partial x_j}\, \mathrm{d}\Omega
=\int_{\Omega}\bu\cdot  {\bf f}\, \mathrm{d}\Omega+
\int_{\partial \Omega}u_iT_{ij}n_j \, \mathrm{d}A.
\end{align}
\end{enumerate}

\hrule 
{\it Useful Lemma:} Let $\bu^i$ be an incompressible flow and $\bu^s$ be a Stokes flow with body force ${\bf f}^s$ such that
\begin{align}
\grad \cdot \bu^i=0 \quad \mathrm{and} \quad
\grad \cdot \bu^s=0, \, \grad \cdot \bT^s=-{\bf f}^s.
\end{align}
Then,
\begin{align}
2\mu\int_{\Omega}e_{ij}^s e_{ij}^i \,\mathrm{d}\Omega
=\int_{\partial\Omega} u_i^i T_{ij}^s n_j\,\mathrm{d}A
+\int_{\Omega}u_i^i f_i^s\,\mathrm{d}\Omega.
\end{align}


{\it Proof:} We know that for a Stokes flow $2\mu e_{ij}^s=T_{ij}^s+p^s \delta_{ij}.$ Hence,
\begin{align}
2\mu e_{ij}^se_{ij}^i=T_{ij}^se_{ij}^i+\underbrace{p^s \delta_{ij}e_{ij}^i}_{=0}
\end{align}
due to incompressibility. Also, using the fact that $T_{ij}$ is symmetric and that $\partial u_i/\partial x_j=e_{ij}+r_{ij}$ where $r_{ij}$ is antisymmetric, we find
\begin{align}
2\mu e_{ij}^se_{ij}^i &=T_{ij}^s\left(\frac{\partial u_i^i}{\partial x_j}-r_{ij}^i\right)=
T_{ij}^s\frac{\partial u_i^i}{\partial x_j}-\underbrace{T_{ij}^sr_{ij}^i}_{=0},\\
& = \frac{\partial}{\partial x_j}\left(T_{ij}^su_i^i\right)-u_i^i\frac{\partial T_{ij}^s}{\partial x_j}.
\end{align}
Integrating over a volume $\Omega$ and applying the divergence theorem then gives the result:
\begin{align}
2\mu\int_{\Omega}e_{ij}^s e_{ij}^i \,\mathrm{d}\Omega
=\int_{\Omega}\frac{\partial}{\partial x_j}\left(T_{ij}^su_i^i\right)\,\mathrm{d}\Omega
-\int_{\Omega}u_i^i\frac{\partial T_{ij}^s}{\partial x_j}\,\mathrm{d}\Omega
=\int_{\partial\Omega} u_i^i T_{ij}^s n_j\,\mathrm{d}A
+\int_{\Omega}u_i^i f_i^s\,\mathrm{d}\Omega.
\end{align}
\hrule 

\begin{enumerate}
\setcounter{enumi}{5}
\item {\bf Uniqueness:}
Uniqueness means that each Stokes flow is unique but can be represented in many ways. Let $\bu^1$ and $\bu^2$ be Stokes flows with the same body forcing and boundary conditions, i.e. ${\bf f}^1={\bf f}^2$ in $\Omega$ and either $\bu^1=\bu^2$ or $\bT^1\cdot{\bf n}=\bT^2\cdot{\bf n}$ on $\partial \Omega$. Then $\bu^1=\bu^2$.

To show this we consider the flow $\bu^*=\bu^1-\bu^2$. From the property of linearity we know that $\bu^*$ must also be a Stokes flow with ${\bf f^*}=\bf 0$ and either $\bu^*=\bf 0$ or $\bT^*\cdot {\bf n}=\bf 0$ on $\partial \Omega$. From the above {\it lemma}, we can show that
\begin{align}
2\mu\int_{\Omega}e_{ij}^*e_{ij}^*\,\mathrm{d}\Omega = 0.
\end{align}
The integrand is positive so must vanish, hence $e_{ij}^*=0$. The flow is strain less so must be in solid body motion ${\bf U}+{\bf \Omega}\times \bx$. Velocity boundary conditions of $\bu^1=\bu^2$ on $\partial \Omega$ imply that $\bu^1=\bu^2$ everywhere.  [N.B. Stress conditions can lead to solid body motion].

\item {\bf Minimum Dissipation:}
Among all incompressible flows with the same velocity boundary conditions, the dissipation is minimised by the Stokes flow with no body force. 

Let $\bu^i$ be an incompressible flow and $\bu^s$ be a Stokes flow with ${\bf f}^s=\bf 0$ and $\bu^i=\bu^s=\bf U$. We know that
\begin{align}
&\, 2\mu\int_{\Omega}(e_{ij}^i-e_{ij}^s)(e_{ij}^i-e_{ij}^s)\,\mathrm{d}\Omega
\geq 0 \quad \mathrm{(positive \,\, integrand)}\\
= & \,2\mu \int_{\Omega}(e_{ij}^ie_{ij}^i-e_{ij}^se_{ij}^s)\,\mathrm{d}\Omega
+\underbrace{4\mu \int_{\Omega}(e_{ij}^s-e_{ij}^i)e_{ij}^s\,\mathrm{d}\Omega}_{=0},
\end{align}
from the {\it lemma} above. Therefore the dissipation is minimised for a Stokes flow
\begin{align}
2\mu\int_{\Omega}e_{ij}^ie_{ij}^i\,\mathrm{d}\Omega \geq
2\mu\int_{\Omega}e_{ij}^se_{ij}^s\,\mathrm{d}\Omega.
\end{align}

\item {\bf Geometric bounding:}
The minimum dissipation theorem can be used to calculate bounds for the flow around more complicated geometries. Consider a cube of side length $2L$ moving at a velocity $\bm{U}$ in a fluid, with drag force $\bm{F}^C$, and Stokes flow $\bu^s(\bx)$ outside the cube.
\begin{figure}[ht!]
\centering
\includegraphics[width=.25\linewidth]{translating_cube.png}
\caption{Stokes flow around a translating cube}
\end{figure}
We can write down the dissipation (using the Stokes flow property that work balances dissipation) as
\begin{align}
\int_{vol. \,outside \,cube}2\mu e_{ij}^s e_{ij}^s\,\mathrm{d}\Omega=
\int_{cube} U_iT_{ij}n_j \,\mathrm{d}A=
\bm{U}\cdot\int_{cube}\bT\cdot \bm{n}\,\mathrm{d}A=-\bm{U}\cdot\bm{F}^C.
\end{align}
We are considering the dissipation in the volume outside of the cube so the normal to the surface points inwards. This is the opposite to calculating drag on the sphere and hence a minus sign is introduced.
Now consider a sphere that just contains the cube so have side length $\sqrt{3}L$. We can define an incompressible flow $\bu(\bx)^i$ outside the cube made up of a Stokes flow outside the sphere and a flow with constant flow velocity $\bm{U}$ between the sphere and the cube:
\begin{align}
\bu^i(\bx)^=\left\{\begin{matrix} \bu(\bx) \quad  (\rm{Stokes \, flow\, past \, sphere}) & \bx\quad \rm{outside\,sphere}\\
\bm{U} & \rm{between \,sphere \,and \,cube}\end{matrix}\right.
\end{align}
The dissipation for this flow is given by
\begin{align}
\int_{vol. \,outside \,cube}2\mu e_{ij}^i e_{ij}^i\,\mathrm{d}\Omega=
\int_{vol. \,outside \,sphere}2\mu e_{ij}^i e_{ij}^i\,\mathrm{d}\Omega=
-\bm{U}\cdot\bm{F}^{sphere}=6\pi \mu \sqrt{3}L\bm{U}\cdot \bm{U},
\end{align}
where we have used the fact that the strain rate for a flow with constant velocity is zero ($\partial U_i/\partial x_j=0$) and the drag on a sphere of radius $a=\sqrt{3}L$ is $\bm{F}=-6\pi\mu a\bm{U}$. Finally, we know that the dissipation is minimised for a Stokes flow, and hence
\begin{align}
6\pi \mu \sqrt{3}L\bm{U}\cdot \bm{U} \geq -\bm{U}\cdot\bm{F}^C.
\end{align}
This gives an upper bound on the drag force on the cube. We can go through a similar argument to find a lower bound by considering the flow outside of a sphere that just fits inside the cube, with radius $a=L$. We won't repeat the analysis here but the result should give 
\begin{align}
6\pi \mu \sqrt{3}L\bm{U}\cdot \bm{U} \geq -\bm{U}\cdot\bm{F}^C \geq
6\pi \mu L\bm{U}\cdot \bm{U}.
\end{align}

\item {\bf Reciprocal theorem:}
Consider Stokes flows $\bu^1(\bx)$ and $\bu^2(\bx)$ in a given volume with different boundary conditions and no body forces. Taking the first flow to be the Stokes flow $\bu^s=\bu^1(\bx)$, the second flow to be the incompressible flow $\bu^i=\bu^2(\bx)$ and using the above $\it lemma$ we have 
\begin{align}
2\mu\int_{\Omega}e_{ij}^1 e_{ij}^2 \,\mathrm{d}\Omega
=\int_{\partial\Omega} u_i^2 T_{ij}^1 n_j\,\mathrm{d}A.
\end{align}
If we instead take the second flow to be the Stokes flow $\bu^s=\bu^2(\bx)$, in a similar manner we have
\begin{align}
2\mu\int_{\Omega}e_{ij}^2 e_{ij}^1 \,\mathrm{d}\Omega
=\int_{\partial\Omega} u_i^2 T_{ij}^1 n_j\,\mathrm{d}A.
\end{align}
Equating the two we find that the rate of working of surface forces of flow 1 against flow 2 equals the rate of working of surface forces of flow 2 against flow 1 (reciprocal theorem):
\begin{align}
\int_{\partial\Omega} u_i^2 T_{ij}^1 n_j\,\mathrm{d}A=
\int_{\partial\Omega} u_i^2 T_{ij}^1 n_j\,\mathrm{d}A.
\end{align}
If the boundary conditions are a uniform translation velocity $\bm{U}^{1,2}$, the reciprocal theorem reduces to 
\begin{align}
\bm{U}^1\cdot\bm{F}^2=\bm{U}^2\cdot\bm{F}^1,
\end{align}
where $\bm{F}^{1,2}$ are the corresponding surface forces.
\end{enumerate}

\section{Stokes Flow Solutions}
\subsection{Representation as Harmonic Functions}
Consider Stokes equations with body force ${\bf f}=\bf 0$: 
\begin{align}
-\grad p+\mu\grad^2\bu={\bf 0}, \quad \grad\cdot \bu =0.
\end{align}
\begin{align}
1. \quad & \grad \cdot \left(-\grad p+\mu\grad^2\bu\right)=0 \quad \Rightarrow \quad
\grad^2 p=0\\
2. \quad & \grad \times \left(-\grad p+\mu\grad^2\bu\right)={\bf 0} \quad \Rightarrow \quad 
\grad^2\bm{\omega}= {\bf 0} \quad\mathrm{where} \quad
\bm{\omega}=\grad\times \bu\\
3. \quad & \grad^2\left(-\grad p+\mu\grad^2\bu\right)={\bf 0} \quad \Rightarrow \quad
\grad^4\bu ={\bf 0}.
\end{align}
Hence, $p$ and $\bm{\omega}$ are harmonic and $\bu$ is biharmonic.

\subsection{Papkovich-Neuber Solutions}
A method for finding the solution of Stokes flows comes from the {\it Papkovich-Neuber} representation. Suppose we can write the pressure $p=\grad^2\Pi$.  Then
\begin{align}
\grad^2(-\grad\Pi+\mu\bu)={\bf 0 } \quad \Rightarrow \quad 
\mu\bu = \grad\Pi -\bm{\Phi} \quad \mathrm{where}\quad
\grad^2\bm{\Phi}={\bf 0}.
\end{align}
Using the incompressibility condition, we have
\begin{align}
\mu\grad\cdot \bu=\grad\cdot(\grad\Pi -\bm{\Phi} )=0 \quad \Rightarrow \quad
\grad^2\Pi=\grad\cdot\bm{\Phi} \quad \Rightarrow \quad
\Pi=\tfrac{1}{2}(\bx\cdot \bm{\Phi}+\chi)\quad \mathrm{where}\quad
\grad^2\chi=0.
\end{align}
We can check this last relation:
\begin{align}
\frac{\partial ^2\Pi}{\partial x_i \partial x_i}&\,=\frac{\partial^2 }{\partial x_i \partial x_i}\left(\frac{1}{2}x_j\Phi_j+\frac{1}{2}\chi\right)=
\frac{1}{2}\frac{\partial }{\partial x_i}\left(\frac{\partial x_j}{\partial x_i}\Phi_j+x_j\frac{\partial \Phi_j}{\partial x_i}\right)+\frac{1}{2}\underbrace{\frac{\partial^2\chi }{\partial x_i \partial x_i}}_{=0}\\
&\,= \frac{1}{2}\frac{\partial }{\partial x_i}\left(\delta_{ij}\Phi_j+x_j\frac{\partial \Phi_j}{\partial x_i}\right)=\frac{1}{2}\left(\frac{\partial \Phi_i}{\partial x_i}+\delta_{ij}\frac{\partial \Phi_j}{\partial x_i}+x_j
\underbrace{\frac{\partial^2 \Phi_j}{\partial x_i\partial x_i}}_{=0}
\right)=\frac{\partial \Phi_i}{\partial x_i}.
\end{align}

\hrule
Any Stokes flow with $\bf f=0$ can be written in terms of a harmonic vector $\bm{\Phi}$ and a harmonic scalar $\chi$:
\begin{align}
2\mu\bu=\grad\left(\bx \cdot \bm{\Phi}+\chi\right)-2\bm{\Phi}
\quad \mathrm{and}\quad
p=\grad\cdot\bm{\Phi}.
\label{PN}
\end{align}

\hrule

\subsection{Spherical Harmonic Solutions}
Spherical harmonic functions are used for solutions involving points, spheres and cylinders that have no preferred direction or orientation. Let the radial coordinate $r=|\bx|\equiv (\bx\cdot\bx)^{1/2}$. We can show that $\grad^2\tfrac{1}{r}=0$, except when $r=0$:
\begin{align}
\frac{\partial^2}{\partial x_j\partial x_j}\left((x_ix_i)^{-1/2}\right)
&\,=-\frac{\partial}{\partial x_j}\left((x_kx_k)^{-3/2}x_i\delta_{ij}\right)
=-\frac{\partial}{\partial x_j}\left((x_kx_k)^{-3/2}x_j\right)\\
&\, = -3(x_kx_k)^{-3/2}+3(x_mx_m)^{-5/2}x_kx_j\delta_{kj}=0.
\end{align}
All other harmonic functions $\phi$ with $\phi\rightarrow 0$ and $r\rightarrow \infty$ are made up of
\begin{align}
\frac{1}{r}, \quad \grad \frac{1}{r}, \quad \grad \grad \frac{1}{r}, \quad \grad \grad \grad \frac{1}{r}, \quad ...
\label{harm1}
\end{align}
Harmonic functions that are bounded at $r=0$ are made up of
\begin{align}
r\frac{1}{r}, \quad r^3\grad \frac{1}{r}, \quad r^5\grad \grad \frac{1}{r}, \quad ...\quad 
r^{2n+1} \grad^{n} \frac{1}{r}, \quad ...
\label{harm2}
\end{align}
These solutions only depend on $r$ and $\bx$ and have no preferred orientation. 
Some useful derivatives to remember are:
\begin{align}
1. \, & (\grad \bx)_{ij} = \frac{\partial x_i}{\partial x_j}=\delta_{ij} \quad \mathrm{(identity)}\\
2. \, & (\grad r)_i = \frac{x_i}{r}\\
3. \, & \left(\grad \frac{1}{r}\right)_i=-\frac{x_i}{r^3}\\
4. \,& (\grad f(r) )_i= f'(r)(\grad r)_i=f'(r)\frac{x_i}{r}
\end{align}

Since we know that Stokes flows are linear in boundary conditions and forcing, we can form Papkovich-Neuber solutions by multiplying them by constant scalars, vectors, tensors, and forming dot-products, for example
\begin{align}
\bm{A}\frac{1}{r}, \quad B\grad\frac{1}{r}, \quad \bm{C}\cdot\grad\frac{1}{r}, \quad ...
\end{align}

\subsection{Tensors and Pseudotensors}

When building these solutions there are some properties it is useful to recall about tensors. An $n$th-rank tensor is a set of objects that transform like a product of $n$ vectors. For example, if $T_{i_1i_2...i_n}$ is an $n$th-rank tensor, then
\begin{align}
T'_{j_1j_2...j_n}=O_{j_1i_1}O_{j_2i_2}...O_{j_ni_n}T_{i_1i_2...i_n},
\end{align}
where $O_{ij}$ is a transformation matrix.
Symmetry properties are tensor invariant, so if $T_{i_1i_2...i_n}$ is symmetric (antisymmetric) under an exchange of $i_1$ and $i_2$ then $T'_{j_1j_2...j_n}$ is also symmetric (antisymmetric) under an exchange of $j_1$ and $j_2$. Examples of tensors in our problems would be 
\begin{align}
\mathrm{velocity}\,\, \bm{u}, \quad \mathrm{force}\,\, 
\bm{F}=\int \bT\cdot\mathbf{n}\,\mathrm{d}A, \quad 
\mathrm{position}\,\, \bx, \quad \mathrm{grad} \,\, \grad, \quad
 \mathrm{identity}\,\, \bm{I}.
\end{align}
In contrast, pseudotensors, such as the Levi-Civita $\epsilon_{ijk}$, change sign under transformation by reflections. Examples of pseudotensors we will come across are
\begin{align}
\mathrm{angular \,\, velocity}\,\, \bm{\Omega}, \quad
\mathrm{couple}\,\, \bm{G}=\int \bx\times(\bT\cdot\mathbf{n})\,\mathrm{d}A,
 \quad
\mathrm{cross\,\, product}\,\, \bm{u}\times\bx, \quad
\mathrm{vorticity}\,\, \bm{\omega}=\grad\times\bu.
\end{align}
If we want to find the velocity $\bu$ using the Papkovich-Neuber representation, then we want $\bm{\Phi}$ and $\chi$ to be tensors.


\subsection{Point Force and Source Flow}
{\bf Point force} (Stokeslet): We want to find the velocity field for a point force. Consider the following:
\begin{align}
\grad\cdot \bT&\, = \mu\grad^2\bu-\grad p=-\bm{F}\delta(\bx)\\
\grad\cdot\bu&\, =0, \quad \bu\rightarrow \bm{0} \quad \mathrm{as} \quad |\bx|\rightarrow \infty.
\end{align}

We know that
\begin{enumerate}
\item[$-$] by linearity, the velocity field must be linear in the forcing $\bm{F}$
\item[$-$] solution must decay in the far field
\item[$-$] there is no preferred direction in the problem, can use spherical harmonics
\item[$-$] the velocity field $\bu$ must be made up of tensors
\end{enumerate}
Using this information, we can now try to build harmonic functions $\bm{\Phi}$ and $\chi$ using the solutions in (\ref{harm1})-(\ref{harm2}). Let's try the following ansatzes:
\begin{align}
\bm{\Phi}&\,=\frac{\alpha\bm{F}}{r} \quad \checkmark\\
\bm{\Phi}&\,=\alpha\bm{F}\cdot \grad \grad \frac{1}{r} \quad \left(\mathrm{equivalently} \quad \chi=\beta\bm{F}\cdot\grad\frac{1}{r}\right)\quad \rightarrow \quad
\mathrm{too\,\,singular\,\,at}\,\,r=0 \quad \times\\
\bm{\Phi}&\,=\alpha\bm{F}\times\grad\frac{1}{r} \quad \rightarrow \quad
\mathrm{pseudotensor} \quad \times
\end{align}
The first ansatz is the only one that decays in the far field and will not blow up at the origin. We proceed by substituting $\bm{\Phi}$ into the solution (\ref{PN}):
\begin{align}
2\mu\bu=\grad\left(\alpha\frac{\bm{F}\cdot\bx}{r}\right)-\frac{2\alpha\bm{F}}{r}
=-\alpha\left(\frac{\bm{F}}{r}+\frac{(\bm{F}\cdot \bx) \bx}{r^3}\right).
\end{align}
To fully specify the solution we need to find $\alpha$. Going back to the problem statement, we want the solution to satisfy
\begin{align}
\grad\cdot\bu=0\quad \Rightarrow \quad \int_{r=R}\bu\cdot\bm{n}\,\mathrm{d}A=0,
\end{align}
(this is built into the Papkovich-Neuber representation), and 
\begin{align}
-\bm{F}\delta(\bx)=\grad\cdot\bT \quad \Rightarrow \quad -\bm{F}=\int_{r=R}\bT\cdot\bm{n}
\,\mathrm{d}A.
\end{align}
On a sphere $r=R$, $\bx = R\bm{n}$, the normal velocity can be calculated
\begin{align}
\bu\cdot\bm{n}|_{r=R}=-\frac{\alpha}{2\mu}\left(\frac{\bm{F}}{R}+\frac{(\bm{F}\cdot R\bm{n})R\bm{n}}{R^3}\right)\cdot \bm{n}=-\frac{\alpha (\bm{F}\cdot\bm{n})}{\mu R}
\end{align}
\begin{align}
\Rightarrow \quad \int_{r=R}\bu\cdot\bm{n}\,\mathrm{d}A=
-\frac{\alpha \bm{F}}{\mu R}\cdot
\underbrace{\int_{r=R}\bm{n}\,\mathrm{d}A}_{=0, \,\, \mathrm{isotropic}}=0.
\end{align}
The normal stress can also be calculated, but requires a bit more algebra:
\begin{align}
\bT\cdot \bm{n}|_{r=R}=(-p\bI+\mu(\grad\bu+\grad\bu^T))\cdot \bm{n}|_{r=R}
\end{align}
with
\begin{align}
p=\grad\cdot\left(\frac{\alpha \bm{F}}{r}\right)=-\frac{\alpha \bm{F}\cdot\bx}{r^3},
\quad \grad\bu=-\frac{\alpha}{2\mu}\left(\frac{(\bm{F}\cdot\bm{x})\bI}{r^3}-
3\frac{(\bm{F}\cdot\bm{x})\bx \, \bx}{r^5}\right),
\end{align}
\begin{align}
\bT\cdot \bm{n}|_{r=R}=\frac{3\alpha(\bm{F}\cdot\bm{n})\bm{n}}{R^2} \quad
\Rightarrow \quad
\int_{r=R}\bT\cdot \bm{n}|_{r=R}\,\mathrm{d}A=\frac{3\alpha\bm{F}}{R^2}\cdot
\underbrace{\int_{r=R}\bm{n}\,\bm{n}\,\mathrm{d}A}_{\bm{A}}
=\frac{3\alpha\bm{F}}{R^2}\cdot\frac{4\pi R^2\bI}{3}
\end{align}
Here, we have noticed that the integral marked $\bm{A}$ is an isotropic second rank tensor so must be $\propto \delta_{ij}$. We can determine the constant of proportionality by calculating the trace, $A_{ii}=4\pi R^2$. Hence, $\alpha=-1/4\pi$. Notice that $\alpha$ is independent of $R$. If we had chosen one of the other harmonic solutions with higher powers of $1/r$, they would be too singular to satisfy the force condition.

\noindent {\bf Source Flow} (Point mass source): We want to find the velocity field for a point source in three-dimensions. Consider the following:
\begin{align}
\grad\cdot \bT&\, = \mu\grad^2\bu-\grad p=\bm{0}\\
\grad\cdot\bu&\, =Q\delta(\bx), \quad \bu\rightarrow \bm{0} \quad \mathrm{as} \quad |\bx|\rightarrow \infty.
\end{align}
Strictly speaking this flow is only incompressible for $r>0$ as volume is not conserved at $r=0$ but generated there instead.

We know that
\begin{enumerate}
\item[$-$] by linearity, the velocity field must be linear in the source flux $Q$
\item[$-$] solution must decay in the far field
\item[$-$] there is no preferred direction in the problem, can use spherical harmonics
\item[$-$] the velocity field $\bu$ must be made up of tensors
\end{enumerate}
Let's try harmonic function
\begin{align}
\chi=\frac{\beta Q}{r} \quad \left(\mathrm{equivalently} \quad \bm{\Phi}=\hat{\beta}Q\grad \frac{1}{r}\right).
\end{align}
In a similar manner to the point force, the other harmonic solutions with higher powers of $1/r$ would be too singular to satisfy the source condition. Substituting into the solution (\ref{PN}) we find:
\begin{align}
2\mu\bu=\grad\left(\frac{\beta Q}{r}\right) \quad \Rightarrow \quad
\bu = -\frac{\beta Q\bx}{2\mu r^3}.
\end{align}
To find $\beta$, we use the source condition at $r=0$:
\begin{align}
\grad\cdot\bu&\, =Q\delta(\bx) \quad \Rightarrow \quad 
Q=\int_{r=R}\bu\cdot\bm{n}\,\mathrm{d}A=
-\frac{\beta Q}{2\mu R^2}\int_{r=R}\bm{n}\cdot\bm{n}\,\mathrm{d}A=-\frac{\beta Q}{2\mu R^2}4\pi R^2.
\end{align}
Hence, $\beta=-\mu/2\pi$. The velocity field then becomes $\bu=Q\bx/4\pi r^3$ which is independent of viscosity $\mu$. This is the three-dimensional extension of the potential flow for a point source studied in \S 6.


%\subsection{Dipoles, Stresslets, Rotlets}

\subsection{Motion of a rigid particle}
A sphere of radius $r=a$ translating with velocity $\mathbf{U}$ and rotating with angular velocity $\mathbf{\Omega}$ moves with velocity 
\begin{align}
\bu|_{r=a}=\mathbf{U}+\mathbf{\Omega}\times \bx
\end{align}
and exerts a force $\mathbf{F}$ and couple $\mathbf{G}$ on the fluid. The dissipation in the fluid due to the particle is (using work balances dissipation)
\begin{align}
\int_{\Omega} \Phi \, \mathrm{d}\Omega
&\,=\int_{\partial\Omega} \bu\cdot \bT\cdot \mathbf{n}\,\mathrm{d}A
=\int_{particle} (\mathbf{U}+\mathbf{\Omega}\times \bx)\cdot \bT\cdot \mathbf{n}\,\mathrm{d}A\\
&\,=\mathbf{U}\cdot\int_{particle} \bT\cdot \mathbf{n}\,\mathrm{d}A+
\mathbf{\Omega}\cdot \int_{particle}
\bx \times (\bT\cdot \mathbf{n})\,\mathrm{d}A\\
&\,=\mathbf{U}\cdot\mathbf{F}+\mathbf{\Omega}\cdot\mathbf{G},
\end{align}
where the normal points out of the fluid into the body, with
\begin{align}
\mathrm{force}\,\, 
\bm{F}=\int_{particle} \bT\cdot \mathbf{n}\,\mathrm{d}A \quad
\mathrm{and}\quad
\mathrm{couple}\,\, 
\bm{G}=\int_{particle}
\bx \times (\bT\cdot \mathbf{n})\,\mathrm{d}A
\end{align}
exerted on the fluid by the sphere.

\subsection{Translating Rigid Sphere}
{\bf Velocity Field:}
We would like to calculate the velocity field for a rigid sphere of radius $r=a$ translating with velocity $\bu=\bm{U}$. Consider the following:
\begin{align}
&\grad\cdot \bT = \mu\grad^2\bu-\grad p=\bm{0}, \quad \grad\cdot\bu =0,\\ 
&\bu=\bm{U} \quad \mathrm{at} \quad r=a, \quad \bu\rightarrow \bm{0} \quad \mathrm{as} \quad |\bx|\rightarrow \infty.
\end{align}

We know that
\begin{enumerate}
\item[$-$] by linearity, the velocity field must be linear in the forcing $\bm{U}$
\item[$-$] solution must decay in the far field
\item[$-$] there is no preferred direction in the problem, can use spherical harmonics
\item[$-$] the velocity field $\bu$ must be made up of tensors
\end{enumerate}
Let's try 
\begin{align}
\frac{\bm{\Phi}}{2\mu}=\frac{\alpha\bm{U}}{r} \quad \mathrm{and} \quad
\frac{\chi}{2\mu}=\beta \bm{U}\cdot\grad\frac{1}{r}.
\end{align}
This then gives velocity field
\begin{align}
\bu = \grad\left(\frac{\alpha \bm{U}\cdot\bx}{r}-\frac{\beta\bm{U}\cdot\bx}{r^3}\right)-\frac{2\alpha\bm{U}}{r} = \alpha \left[-\frac{\bm{U}}{r}-\frac{(\bm{U}\cdot\bx)\bx}{r^3}\right]
+\beta\left[-\frac{\bm{U}}{r^3}+\frac{3(\bm{U}\cdot\bx)\bx}{r^5}\right].
\end{align}
Applying the boundary condition $\bu=\bm{U}$ at $r=a$ gives
\begin{align}
1=-\frac{\alpha}{a}-\frac{\beta}{a^3}, \quad 0=-\frac{\alpha}{a}+\frac{3\beta}{a^3} 
\quad \Rightarrow \quad \alpha = -\frac{3a}{4}, \quad \beta = -\frac{a^3}{4},
\end{align}
and
\begin{align}
\bu = \frac{3\bm{U}}{4}\left(\frac{a}{r}+\frac{a^3}{3r^3}\right)+
\frac{3(\bm{U}\cdot\bx)\bx}{4}\left(\frac{a}{r^3}-\frac{a^3}{r^5}\right).
\end{align}
The pressure field can also be calculated
\begin{align}
p=-\frac{3\mu a\bm{U}}{2}\cdot\grad\frac{1}{r}=\frac{3\mu a(\bm{U}\cdot\bx)}{2r^3}.
\end{align}

When finding the velocity field for the point force and point source, we were restricted with the possible harmonic functions to choose from because of the behaviour at $r=0$. For the translating sphere this is no longer the case. You will notice that if we set either $\alpha$ or $\beta$ to zero we can no longer satisfy the boundary condition on $r=a$. 

\noindent {\bf Force and stress on the sphere:}
We start by calculating the stress $\bT$ from the velocity and pressure fields,
\begin{align}
\bT =&\, -p\bI+\mu\left(\grad\bu +\grad \bu^T\right)\\
=&\,-\frac{3\mu a(\bm{U}\cdot\bx)\bI}{2r^3}+
\mu\left\{\frac{3}{4}\left(-\frac{a}{r^3}-\frac{a^3}{r^5}\right)(\bm{U}\,\bx+\bx\,\bm{U})
+\frac{3}{2}\left(\frac{-3a}{r^5}+\frac{5a^3}{r^7}\right)(\bm{U}\cdot\bx)\bx\,\bx\right.\\
&\left.+\frac{3}{4}\left(\frac{a}{r^3}-\frac{a^3}{r^5}\right)\left[2(\bm{U}\cdot\bx)\bI+\bm{U}\,\bx+\bx\,\bm{U}\right]\right\}
\end{align}
On $r=a$, $\bx = a\bm{n}$, the stress is
\begin{align}
\bT\cdot\bm{n}|_{r=a}=-\frac{3\mu\bm{U}}{2a}.
\end{align}
The stress on the sphere is constant. Calculating the force, or drag, on the sphere is then relatively straightforward
\begin{align}
\mathrm{drag}=\bm{F}=\int_{r=a}-\frac{3\mu\bm{U}}{2a}\,\mathrm{d}A=-6\pi\mu a \bm{U}.
\end{align}

\noindent{\bf Gravitational settling:}
Now that we have the drag on a sphere translating at a given velocity, we can reverse the problem and ask what is the settling speed of a sphere falling under gravity.
\begin{figure}[ht!]
\centering
\includegraphics[width=.25\linewidth]{sedimenting_sphere.png}
\caption{Sketch of settling sphere with forces.}
\end{figure}
Balancing the forces on a steady fall we have
\begin{align}
\underbrace{\bm{0}}_{(1)}=
\underbrace{\frac{4\pi a^3\rho\bg}{3}}_{(2)}-
\underbrace{\frac{4\pi a^3\rho_{f}\bg}{3}}_{(3)}
-\underbrace{6\pi\mu a \bm{U}}_{(4)} \quad \Rightarrow \quad 
\bm{U}=\frac{2\Delta \rho a^2\bg}{9\mu}.
\end{align}
where
\begin{enumerate}
\item no inertia (steady fall, no acceleration)
\item weight of the sphere, density $\rho$
\item buoyancy (weight of displaced fluid, density $\rho_{f}$)
\item Stokes drag
\end{enumerate}
E.g. For a ball bearing with density $\rho=8g/cm^3$ we can calculate the sedimenting speed for different fluids:
\begin{center}
\begin{tabular}{lllllll}
      &{$a$}       & {$\rho_f$}     &{$\Delta \rho$}   & {$\mu$}   &{$U$} &{$\rm Re$}\\
 Water:	&{$1\,\mu m$}& $1\,g/cm^3$ &$7\,g/cm^3$&$10^{-3}\, Pa s$ & $15\,\mu m/s$&$10^{-4} \ll 1$\\
 Golden syrup: &{$1\,m m$}& $1.4\,g/cm^3$ &$6.6\,g/cm^3$& 60 $Pa s$&$0.24\,mm/s$&$3\times 10^{-5}\ll 1$
\end{tabular}
\end{center}
This experiment can be used to calculate the viscosity of different fluids. 

\noindent{\bf Far-field approximation:}
Far away from the sphere, we can show that the disturbance looks like a point force. The velocity field when $r\rightarrow \infty$ is
\begin{align}
\bu = \frac{3a\bm{U}}{4r}+
\frac{3a(\bm{U}\cdot\bx)\bx}{4r^3}.
\end{align}
The force exerted across `$r=\infty$' must balance the force exerted across $r=a$ so the strength of the point force ${\bm{F}}'=6\pi\mu a {\bm{U}}$. Substituting this into the velocity field gives
\begin{align}
\bu = \frac{1}{8\pi\mu}\left(\frac{\bm{F}'}{r}+
\frac{(\bm{F}'\cdot\bx)\bx}{r^3}\right).
\end{align}
This disturbance flow only knows the total force exerted and is independent of particle radius $a$.

