\chapter{Dimensional Analysis}
Dimensional analysis applies the principles of manipulating dimensions of physical quantities for reducing the complexity of a problem. It is best illustrated through examples.

\begin{figure}[h]
\centerline{\includegraphics[width=60mm]{Cylinder}}
\caption{Schematic of a two-dimensional flow past a cylinder.}
\label{fig:Cylinder}
\end{figure}
\section{Reducing parameters in a set of equations}
A process called ``non-dimensionalization'' is used to reduce the number of parameters the solution of a set of equations depend on. Here, we non-dimensionalize the Navier-Stokes equations for flow past an infinitely-long cylinder.

Consider a fluid of density $\rho$ and viscosity $\mu$ flowing with speed $U$ along the $x$-axis past an infinitely long cylinder of radius $R$, as shown in Figure~\ref{fig:Cylinder}. Let us write the Navier-Stokes equations and the boundary conditions. 
\begin{subequations}
\begin{align}
\pd{u}{x} + \pd{v}{y} &= 0, \label{eqn:CylinderContinuity}\\
\rho\left( \pd{u}{t} + u\pd{u}{x} + v \pd{u}{y} \right) &= -\pd{p}{x} + \mu \left( \pdd{u}{x} + \pdd{u}{y} \right), \label{eqn:Cylinderxmom}\\
\rho\left( \pd{v}{t} + u\pd{v}{x} + v \pd{v}{y} \right) &= -\pd{p}{y} + \mu \left( \pdd{v}{x} + \pdd{v}{y} \right), \label{eqn:Cylinderymom}\\
\bu(\bx\to \infty, t) &= U \e_x, \label{eqn:CylinderFarfield} \\
\bu(|\bx|=R, t) &= \boldsymbol{0}. \label{eqn:CylinderNoslip}
\end{align}
\label{eqn:CylinderNS}
\end{subequations}
We would like to determine the drag, $D$, per unit length on the cylinder
\begin{align}
D = \int_{\partial\Omega} \e_x\cdot\bT\cdot\nhat~dA, \quad \text{where} \quad T_{ij} = -p \delta_{ij} + \mu \left( \pd{u_i}{x_j} + \pd{u_j}{x_i} \right). \label{eqn:CylinderDrag}
\end{align}

First notice the parameters of this problem: $\rho$, $\mu$, $U$ and $R$. We use three of these parameters ($R$, $U$ and $\rho$) to ``non-dimensionalize'' all the quantities in the problem, i.e., we rescale
\begin{align}
t = \dfrac{R}{U} \tr{t}, \quad \bx = R\tr{\bx}, \quad \bu = U\tr{\bu}, \quad p = \rho U^2 \tr{p}, \quad \text{and} \quad \bT = \rho U^2 \tr{\bT}.
\end{align}
Note that $\tr{x}$, $\tr{\bu}$, $\tr{p}$ and $\tr{\bT}$ are all dimensionless.

This transformation converts \eqref{eqn:CylinderNS} to
\begin{subequations}
\begin{align}
\pd{\tr{u}}{\tr{x}} + \pd{\tr{v}}{\tr{y}} &= 0, \label{eqn:CylinderContinuityND}\\
\left(\pd{\tr{u}}{\tr{t}} + \tr{u}\pd{\tr{u}}{\tr{x}} + \tr{v} \pd{\tr{u}}{\tr{y}} \right) &= -\pd{\tr{p}}{\tr{x}} + \dfrac{1}{\Rey} \left( \pdd{\tr{u}}{\tr{x}} + \pdd{\tr{u}}{\tr{y}} \right), \label{eqn:CylinderxmomND}\\
\left(\pd{\tr{v}}{\tr{t}} + \tr{u}\pd{\tr{v}}{\tr{x}} + \tr{v} \pd{\tr{v}}{\tr{y}} \right) &= -\pd{\tr{p}}{\tr{y}} + \dfrac{1}{\Rey} \left( \pdd{\tr{v}}{\tr{x}} + \pdd{\tr{v}}{\tr{y}} \right), \label{eqn:CylinderymomND}\\
\tr{\bu}(\tr{\bx}\to \infty, \tr{t}) &= \e_x, \label{eqn:CylinderFarfieldND} \\
\tr{\bu}(|\tr{\bx}|=1, t) &= \boldsymbol{0}, \label{eqn:CylinderNoslipND}
\end{align}
\label{eqn:CylinderNSND}
\end{subequations}
where $\Rey = \dfrac{\rho U R}{\mu}$ is called the Reynolds number after Osborne Reynolds. The Reynolds number is the ratio of the strengths of inertial effects to viscous effects in the fluid.

The rescaled version of drag from \eqref{eqn:CylinderDrag} is
\begin{align}
D = \rho U^2 R \int_{\partial\Omega} \e_x\cdot\bT\cdot\nhat~dA, \quad \text{where} \quad \tr{T}_{ij} = -\tr{p} \delta_{ij} + \dfrac{1}{\Rey} \left( \pd{\tr{u}_i}{\tr{x}_j} + \pd{\tr{u}_j}{\tr{x}_i} \right). \label{eqn:CylinderDragND}
\end{align}

Note that the dimensionless equations \eqref{eqn:CylinderNSND} are equivalent to the dimensional one \eqref{eqn:CylinderNS} with $\rho=1$, $U=1$, $R=1$ and $\mu = \Rey^{-1}$. This means the flows for different sets of parameters are simply rescaled versions of each other if the $\Rey$ for them is identical. Using this, the drag on these cylinders will also be related. Or, rewriting \eqref{eqn:CylinderDragND} as
\begin{align}
C_D = \dfrac{D}{\frac{1}{2} \rho U^2 R} = 2 \int_{\partial\Omega} \e_x\cdot\bT\cdot\nhat~dA, \label{eqn:CylinderCD}
\end{align}
implies that 
\begin{align}
C_D = F(\Rey) \text{ alone.}
\label{eqn:cd}
\end{align}
Here $C_D$ is called the drag coefficient, and the factor of 1/2 in the denominator is for historical reasons.

\section{Deducing dependence on parameters}
Equation \ref{eqn:cd} may be derived without reference to partial differential equations, simply by using the arbitrariness of the system of units. Clearly, the drag on the cylinder depends on $R$, $U$, $\mu$ and $\rho$, i.e.,
\begin{align}
 D = \dfrac{1}{2} f(\rho, \mu, R, U),
 \label{eqn:dragdim}
\end{align}
where the factor of 1/2 is included to conform to convention that will be introduced later.
In a different set of units, where length $l$, time $t$ and mass $m$ transforms as
\begin{align}
\tr{l} = \alpha l, \qquad \tr{t} = \beta t, \qquad \tr{m} = \gamma{m},
\end{align}
where the symbols with the tilde refer to transformed units. Under this transformation, the parameters transform as
\begin{align}
 &\tr{\rho} = \dfrac{\tr{m}}{\tr{l}^3} = \dfrac{\gamma}{\alpha^3} \dfrac{m}{l^3} = \dfrac{\gamma}{\alpha^3} \rho, \\
 &\tr{\mu}  = \dfrac{\tr{m}}{\tr{l}\tr{t}} = \dfrac{\gamma}{\alpha \beta} \dfrac{m}{lt} = \dfrac{\gamma}{\alpha \beta} \mu, \\
 &\tr{R} = \alpha R, \\
 &\tr{U} = \dfrac{\alpha}{\beta} U, \text{ and} \\
 &\tr{D} = \dfrac{\gamma}{\beta^2} D.
\end{align}
By the arbitrariness of system of units, \eqref{eqn:dragdim} also holds in the transformed system, i.e.,
\begin{align}
 \tr{D} = \dfrac{1}{2} f(\tr{\rho}, \tr{\mu}, \tr{R}, \tr{U}).
 \label{eqn:dragdimtr}
\end{align}
Transforming it back implies
\begin{align}
\dfrac{\gamma}{\beta^2} D = \dfrac{1}{2} f\left(\dfrac{\gamma}{\alpha^3} \rho,~ \dfrac{\gamma}{\alpha \beta} \mu,~ \alpha R,~  \dfrac{\alpha}{\beta} U\right).
\end{align}
Because the scaling factors $\alpha$, $\beta$ and $\gamma$ could be chosen at will, we choose
\begin{align}
 \alpha = 1/R, \qquad \beta = \dfrac{U}{R}, \qquad \gamma=\dfrac{1}{\rho R^3}.
\end{align}
Substituting in \eqref{eqn:dragdimtr} gives
\begin{align}
 \dfrac{D}{\frac{1}{2}\rho U^2 R } = f\left(1, \dfrac{\mu}{\rho U R}, 1, 1 \right) = F(\Rey),
\end{align}
where the inserted factor of $1/2$ turns the left hand side to the drag coefficient as in \eqref{eqn:cd}.

First, let us derive \eqref{eqn:cd} using a short-cut. Notice that there are two ways to construct the dimensions of $D$ from those of $\rho$, $\mu$, $R$ and $U$: (i) $\rho U^2 R$, and (ii) $\mu U$. The former neglects viscosity and the latter inertia. Since both have dimensions of $D$, their ratio is dimensionless; in fact
\begin{align}
\dfrac{\rho U^2 R}{\mu U} = \Rey.
\end{align}
Because there are 3 basic dimensions of mass, length and time that make up the dimensions of 4 parameters, there is only 4-3=1 dimensionless parameter that could be constructed from the 4 parameters. 

We can go a little further and derive more insight into the functional form of $F(\Rey)$. Suppose that $\Rey$ is large, so inertia dominates and viscous effects are negligible. Under these circumstances, $\mu$ is eliminated as a parameter, so
\begin{align}
D = f_I(\rho, U, R) = \dfrac{1}{2} C_D \rho U^2 R.
\end{align}
In the opposite extreme, if the fluid is extremely viscous, $\Rey$ is small, the density of the fluid (which quantifies inertia) could be neglected and
\begin{align}
D = f_V(\mu, R, R) = C_V \mu U,
\end{align}
where $C_V$ is a dimensionless constant.
The relation could be rearranged to determine the functional form of $F(\Rey)$ in the limit of small or large $\Rey$.
\begin{align}
 F(Re) &= C_D = \text{constant} \qquad & \dots&~\Rey \text{ large}, \\
       &= \dfrac{C_V \mu U}{\frac{1}{2} \rho U^2 R} = \dfrac{2C_V}{\Rey} \qquad & \dots&~\Rey \text{ small}. 
\end{align}

In this way, dimensional analysis can assist in reducing the number of parameters in the physical relation between variables describing a phenomenon.

