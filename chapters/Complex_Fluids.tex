\chapter{Complex Fluids and Non-Newtonian Rheology}\label{ch:complexfluids}
\section{Constitutive laws}
Up until now, we have considered Newtonian fluids with a linear relationship between the stress and rate of deformation in the fluid. For fluids consisting of small molecules, like water, this approximation works very well, even in turbulent flows with high flow rates. 
In realistic Newtonian flows, the separation of length and time scales mean the typical intermolecular distances or velocity distributions of individual constituents are the same whether the flow is turbulent or at rest. Hence, the energy dissipation in the fluid, which is represented by viscosity in the Newtonian constitutive law, is not affected by the flow.

When the applied flows are capable of altering the local microstructure of the fluid the classical Newtonian approximation might fail to provide an adequate mathematical model of the dynamics. For example, this is the case in solutions of colloidal particles, long flexible polymers, wormlike micelles, and similar complex fluids. These particles are significantly larger than individual molecules of typical Newtonian fluids, and the time scales of stress relaxation in these complex fluids are significantly longer than in their Newtonian counterparts. One can use the longest relaxation time $\lambda$ to form a dimensionless group 
\begin{equation}
\mathrm{Wi}=\lambda \dot{\gamma}, \qquad \dot{\gamma}=\sqrt{\tfrac{1}{2}\Sigma_{jk}\dot{\gamma}_{jk}^2},
\end{equation}
the Weissenberg number. Here the shear rate $\dot{\gamma}$ is an invariant measure of the rate of strain in the fluid (we will talk about this more in detail below).

For small velocity gradients, $\mathrm{Wi}\ll 1$, complex fluids obey the linear constitutive law, and
flow like Newtonian fluids at the same Reynolds number. When the Weissenberg number is comparable to or larger than unity, complex fluids exhibit non-Newtonian behaviour and obey complicated constitutive models, often involving non-linear dependence of the local stress on the velocity gradient and the deformation history of the fluid. In this chapter we will first consider the simplest extension of a Newtonian description in which the flow only influences the instantaneous viscosity of the fluid. We will then discuss a general theory dealing with history-dependent properties of viscoelastic fluids.


\subsection{Newtonian fluid}
To begin with, let's recap what we know for Newtonian fluids. The fluid is Newtonian if the relationship between the rate of deformation ($\gamma_{ij}=\partial u_i/\partial x_j+\partial u_j/\partial x_i$) and the stress $\bT$ is local, linear, instantaneous and isotropic. If the fluid is also incompressible, the Newtonian constitutive law is
\begin{align}
\bT=-p\bI+\mu \left(\grad \bu+\grad \bu^T\right)\equiv -p\bI+\underbrace{\mu\boldsymbol{\dot{\gamma}}}_{\boldsymbol\tau},
\end{align}
where $\mu$ is constant and $\boldsymbol\tau$ is the deviatoric stress tensor and represents the traceless ($\grad\cdot \bu=0$) part of the stress tensor, while the pressure is the isotropic part.
\newcommand{\dgam}{\dot{\gamma}}
\newcommand{\bdgam}{\boldsymbol{\dot{\gamma}}}

\subsection{Generalised Newtonian fluid}
\begin{figure}
\centering
\includegraphics[width=.8\linewidth]{rheol.png}
\caption{\label{rheol}1D constitutive laws for Newtonian, power-law and yield stress fluids with typical examples.}
\end{figure}
A generalised Newtonian fluid assumes that the applied flow only changes the energy dissipation in the fluid, represented by the viscosity, but does not change the tensorial structure of the Newtonian model. The generalised constitutive law is given by
\begin{align}
\bT=-p\bI+\mu(\bdgam)\bdgam,
\end{align}
where $\mu(\bdgam)$ is the viscosity now dependent on the fluid flow. To ensure that the viscosity does not change under a coordinate transformation, which would be unphysical, the viscosity can only depend on invariants of the strain-rate tensor $\bdgam$.
The second tensorial invariant is the lowest non-trivial invariant, so we may write
\begin{align}
\bT=-p\bI+\mu(\dgam)\bdgam, \qquad \dgam=\sqrt{\tfrac{1}{2}\Sigma_{jk}\dot{\gamma}_{jk}^2}.
\end{align}

\noindent \bf{Power-law fluid}\\\normalfont
For a power-law fluid the viscosity is given by
\begin{align}
\mu(\dgam)=\hat{\mu}|\dgam|^{n-1}.
\end{align}
When $n=1$ this reduces to a Newtonian fluid. When $n<1$ the fluid is {\it shear-thinning} and the effective viscosity decreases with the rate of deformation; when $n>1$ the fluid is {\it shear-thickening} and the effective viscosity increases with the rate of deformation. Figure \ref{rheol} gives some examples of these fluids. Note, the units of $\hat{\mu}$ are $PT^n$ ($P$-pressure, $T$-time). The constitutive law for a power-law fluid is then written as
\begin{align}
\btau=\hat{\mu}|\dgam|^{n-1}\bdgam.
\end{align}

\noindent \bf{Yield stress fluid}\normalfont \\
Yield stress fluids incorporate both solid-like and fluid like behaviour. They are characterised in terms of a yield stress $\tau_Y$. When the stress is below this yield stress there is no deformation, $\bdgam=\bf{0}$. When the stress is above this yield stress the fluid flows with some viscosity dependent on the strain-rate invariant. 

For example, a {\it Bingham fluid} has constitutive law
\begin{align}
\btau=\left(\frac{\tau_Y}{\dgam}+\mu\right)\bdgam, \qquad \tau=\sqrt{\tfrac{1}{2}\Sigma_{jk}\tau_{jk}^2}>\tau_Y,
\end{align}
and $\bdgam=\bf{0}$ otherwise, where $\mu$ is a constant viscosity. Note, $\tau$ is the second invariant of the deviatoric stress tensor $\btau$. This is chosen to decide whether the fluid is yielded or not as it does not change under a coordinate transformation and the first invariant is zero.

A {\it Herschel-Bulkley fluid} has constitutive law
\begin{align}
\btau=\left(\frac{\tau_Y}{\dgam}+K|\dgam|^{n-1}\right)\bdgam, \qquad \tau=\sqrt{\tfrac{1}{2}\Sigma_{jk}\tau_{jk}^2}>\tau_Y,
\end{align}
and $\bdgam=\bf{0}$ otherwise, where $K$ is the consistency with units $PT^n$ and $n$ is the power-law index.

\section{Poiseuille flow}

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{pipe.png}
\caption{\label{pipe}Sketch of Poiseuille flow through a two-dimensional pipe.}
\end{figure}

As a simple example, we will consider Poiseuille flow through a two-dimensional pipe. Figure \ref{pipe} shows the set up of a pipe with pressure gradient $P_x$ driving the flow at one end $x=0$. $x$ is the along pipe coordinate; $y$ in the across pipe coordinate with $-h\leq y\leq h$. The velocity $\bu=(u,v)$ then satisfies the following conservation of momentum and mass equations:
\begin{align}
\rho\left(\frac{\partial u}{\partial t}+u\frac{\partial u}{\partial x}+v\frac{\partial u}{\partial y}\right)=&\,
\frac{\partial T_{xx}}{\partial x}+\frac{\partial T_{xy}}{\partial y}\\
\rho\left(\frac{\partial v}{\partial t}+u\frac{\partial v}{\partial x}+v\frac{\partial v}{\partial y}\right)=&\,
\frac{\partial T_{xy}}{\partial x}+\frac{\partial T_{yy}}{\partial y}\\
\frac{\partial u}{\partial x}+\frac{\partial v}{\partial y}=&\, 0.
\end{align}
We look for steady solutions where the flow is all in the $x$-direction and only varies in $y$ such that $\bu=(U(y),0)$. Mass conservation is automatically satisfied and the momentum equations reduce to
\begin{align}
0=&\, \frac{\partial T_{xx}}{\partial x}+\frac{\partial T_{xy}}{\partial y}\\
0=&\, \frac{\partial T_{xy}}{\partial x}+\frac{\partial T_{yy}}{\partial y}.
\end{align}
The only non-zero component of the strain-rate tensor is $\dgam_{xy}=dU/dy$, and hence $\dgam=|dU/dy|$. The stress components are therefore
\begin{align}
T_{xx}=T_{yy}=-p, \quad T_{xy}=\tau_{xy} \quad \Rightarrow \quad
0=-\frac{\partial p}{\partial x}+\frac{\partial \tau_{xy}}{\partial y} \quad \mathrm{and}
\quad 0=-\frac{\partial p}{\partial y}.
\end{align}
Hence, $p$ is only a function $x$. Since $\tau_{xy}$ is only a function of $y$, $p$ must also be linear in $x$:
\begin{align}
p=p_0-P_x x \quad \Rightarrow \quad -P_x=\frac{d \tau_{xy}}{dy}.
\label{pois}
\end{align}
We have yet to consider the rheology of the fluid which we will need to do to solve for the flow field.

\noindent \bf{Newtonian fluid}\\\normalfont
For a Newtonian fluid, 
\begin{align}
\tau_{xy}=\mu\dgam_{xy}=\mu \frac{dU}{dy}.
\end{align}
Substituting this into (\ref{pois}) gives a second order ODE for $U$ subject to two boundary conditions: no-slip on the walls $U(\pm h)=0$. Solving for the velocity we find
\begin{align}
U(y)=\frac{P_x}{2\mu}(h^2-y^2).
\end{align}

\noindent \bf{Power-law fluid}\\\normalfont
For a power-law fluid, we have
\begin{align}
\tau_{xy}=\hat{\mu}\left|\frac{dU}{dy}\right|^{n-1}\frac{dU}{dy}.
\end{align}
Substituting this into (\ref{pois}) again gives a second order ODE for $U$ subject to two boundary conditions: no-slip on the walls $U(\pm h)=0$. We have
\begin{align}
\hat{\mu}\left|\frac{dU}{dy}\right|^{n-1}\frac{dU}{dy}=-P_xy+A.
\end{align}
By symmetry, we know $A=0$. Upon integrating we find velocity
\begin{align}
U(y)=\frac{n}{n+1}\left(\frac{P_x}{\hat{\mu}}\right)^{1/n}\left[h^{(n+1)/n}-|y|^{(n+1)/n}\right].
\end{align}
Note, you need to be careful of signs here, in particular it's useful to know that $\mathrm{sgn}(dU/dy)=-\mathrm{sgn}(y)$.

\noindent \bf{Bingham fluid}\\ \normalfont
For a Bingham fluid, we have
\begin{align}
\tau_{xy}=\left(\frac{\tau_Y}{\dgam}+K\right)\dgam_{xy}=\tau_Y\mathrm{sgn}\left(\frac{dU}{dy}\right)+K\frac{dU}{dy}=-P_x y \quad \mathrm{for}\quad \tau= |\tau_{xy}|>\tau_Y
\end{align}
The yield condition is equivalent to $|y|>\tau_Y/P_x$. Hence, we need to solve for $U$ in three different regions:

1. $\tau_Y/P_x<y<h$ where $dU/dy<0$. This then gives
\begin{align}
-\tau_Y+K\frac{dU}{dy}=-P_x y \quad \Rightarrow \quad 
U(y)=\frac{P_x}{2K}(h^2-y^2)-\frac{\tau_Y}{K}(h-y).
\end{align}

2. $-h<y<-\tau_Y/P_x$ where $dU/dy>0$. This then gives
\begin{align}
\tau_Y+K\frac{dU}{dy}=-P_x y \quad \Rightarrow \quad 
U(y)=\frac{P_x}{2K}(h^2-y^2)-\frac{\tau_Y}{K}(h+y).
\end{align}

3. $-\tau_Y/P_x<y<\tau_Y/P_x$ where $dU/dy=0$. By matching onto the velocities either side of the unyielded region ($|\tau_{xy}|<\tau_Y$) we find plug velocity
\begin{align}
U(y)=U_p=\frac{P_x h^2}{2K}\left[1-\frac{\tau_Y}{P_x h}\right]^2
\end{align}
which is constant.
Figure \ref{plug} plots the velocity profiles for Power-law and Bingham fluids. The central region is {\it unyielded} and flows in a solid plug. When $\tau_Y/P_xh=1$ the whole flow is unyielded and the plug velocity is zero i.e. the pressure gradient is not sufficient to get the flow to yield. Yield surfaces divide the unyielded and yielded parts of the flow. In this case the yield surfaces are given by $|y|=Y\equiv \tau_Y/P_x$.

\begin{figure}
\centering
\includegraphics[width=.8\linewidth]{plug.png}
\caption{\label{plug}Velocity profile for Power-law and Bingham constitutive laws.}
\end{figure}

\section{Taylor-Couette flow}
\begin{figure}
\centering
\includegraphics[width=.3\linewidth]{cou.png}
\caption{\label{cou}Sketch of Taylor-Couette flow between two concentric cylinders. }
\end{figure}

A second example is a Taylor-Couette device where we have fluid between two concentric cylinders. The inner cylinder at $r=a$ is fixed and the outer cylinder at $r=b$ rotates at a constant rotation rate $\Omega$, see figure \ref{cou}. For an axisymmetric steady flow, the velocity has the form $\bu=(0,u_{\theta}(r),0)$. The only non-zero strain-rate component is $\dgam_{r\theta}$, and hence $T_{r\theta}=\tau_{r\theta}$ is the only non-zero deviatoric stress component. In cylindrical polar coordinates, the strain rate tensor and conservation of momentum in the azimuthal direction are given by
\begin{align}
\dgam_{r\theta}=\frac{du_{\theta}}{dr}-\frac{u_{\theta}}{r}=r\frac{d}{dr}\left(\frac{u_{\theta}}{r}\right), \quad \frac{d}{dr}\left(\tau_{r\theta}\right)+\frac{2}{r}\tau_{r\theta}
=\frac{1}{r^2}\frac{d}{dr}\left(r^2\tau_{r\theta}\right)=0.
\end{align}
N.B. Mass conservation is automatically satisfied by the form of the assumed velocity field.
This can be integrated to give stress
\begin{align}
\tau_{r\theta}=\frac{A}{r^2},
\label{cou1}
\end{align}
for some constant $A$.

\noindent{\bf Bingham fluid}\\
For a Bingham fluid, the constitutive law is
\begin{align}
\begin{matrix}
\tau_{r\theta}=\left(\frac{\tau_Y}{|\dgam_{r\theta}|}+\mu\right)\dgam_{r\theta}
=\tau_Y+\mu\dgam_{r\theta}, &  \tau_{r\theta}>\tau_Y\\
\dgam_{r\theta}=0, &  \tau_{r\theta}<\tau_Y,
\end{matrix}
\end{align}
where $\dgam_{r\theta}>0$ because the inner cylinder is held fixed and the outer cylinder is rotating with rotation rate $\Omega >0$. Substituting this into (\ref{cou1}), in the yielded region, $a<r<r_Y$, we have
\begin{align}
\frac{A}{r^2}-\tau_Y=\mu\dgam_{r\theta}=\mu r\frac{d}{dr}\left(\frac{u_{\theta}}{r}\right), \quad \frac{A}{r_Y^2}-\tau_Y=0
\end{align}
and in the unyielded region $r_Y<r<b$ we have
\begin{align}
\dgam_{r\theta}=0 \quad \Rightarrow \quad
\frac{u_{\theta}}{r}=\Omega.
\end{align}
Upon integrating and applying boundary condition $u_{\theta}(a)=0$  we find
\begin{align}
\frac{u_{\theta}}{r}=\left\{\begin{matrix}
\frac{\tau_Y r_Y^2 }{2\mu}\left(\frac{1}{a^2}-\frac{1}{r^2}\right)-\frac{\tau_Y}{\mu}\ln\left(\frac{r}{a}\right) &a<r<r_Y\\
\Omega
& r_Y<r<b,\end{matrix}\right.
\end{align}
where matching at yield surface gives relationship between the rotation rate and $r_Y$
\begin{align}
\Omega=\frac{\tau_Y r_Y^2 }{2\mu}\left(\frac{1}{a^2}-\frac{1}{r_Y^2}\right)-\frac{\tau_Y}{\mu}\ln\left(\frac{r_Y}{a}\right).
\end{align}

Here, we have assumed that we can rotate the outer cylinder with some fixed rotation rate $\Omega$ and cause it to yield irrespective of how large the yield stress of the Bingham fluid is. In reality, we will apply some torque $\bG=G\hat{\mathbf{z}}$ to the outer cylinder and the rotation rate will depend on the magnitude of the torque, the rheology of the fluid, and the size of the cylinder. The torque on the outer cylinder is defined as
\begin{align}
\bG=\int \br\times (\bT\cdot\bn)\, \mathrm{d}A=\int \br\times (\bT\cdot\bn)\, b\mathrm{d}\theta\mathrm{d}z,
\end{align}
where we have used surface element $\mathrm{d}A=r\mathrm{d}\theta\mathrm{d}z$. We know that on the outer cylinder $\br=(b,0,0)=b\hat{\br}$, $\bT\cdot\bn=(0,\tau_{r\theta}|b,0)=\tau_{r\theta}|_b\hat{\boldsymbol{\theta}}$ and $\hat{\br}\times\hat{\boldsymbol{\theta}}=\hat{\mathbf{z}}$. Hence,
\begin{align}
\bG=\hat{\mathbf{z}}\int_0^L\int_0^{2\pi}\tau_{r\theta}|_bb^2\, \mathrm{d}\theta\mathrm{d}z
=2\pi L b^2\tau_{r\theta}|_b\hat{\mathbf{z}} \quad \Rightarrow \quad
\tau_{r\theta}|_b=\frac{G}{2\pi L b^2}.
\end{align}
Comparing this with the stress $\tau_{r\theta}=\tau_Y r_Y^2/r^2$ calculated above, we find a relationship between the yield surface and the magnitude of the torque $G$
\begin{align}
\tau_{r\theta}|_b=\tau_Y r_Y^2/b^2=\frac{G}{2\pi L b^2}
\quad \Rightarrow \quad
r_Y=\sqrt{\frac{G}{2\pi L\tau_Y}}.
\end{align}
If $a<\sqrt{G/{2\pi L\tau_Y}}<b$, then we have the velocity structure as calculated above with a yield surface within the domain. If $\sqrt{G/{2\pi L\tau_Y}}<a$ then the fluid is unyielded everywhere and performs solid body rotation. (N.B. if solid body rotation does occur everywhere then the rotation rate at the inner cylinder is also $\Omega>0$, which does not satisfy the fixed boundary condition there. Instead there must be a yielded boundary layer near the inner wall). If $\sqrt{G/{2\pi L\tau_Y}}>b$, the fluid is yielded everywhere. 

\section{Viscoelastic fluids}
When experiments are done with polymeric fluids it is observed that the response to the shear rate is not always reproducible even if the same stress is reached. In this case, it takes time for the fluid to adjust to the flow with fluid having some memory of its flow history. A polymeric fluid will not remember its history forever, but because the molecules can be stretched by the flow it has some memory until the molecules relax. These fluids incorporate the behaviours of viscous fluids (which are instantaneous and have no memory) and elastic solids (which remember everything). Their combination gives a {\it viscoelastic liquid} that has a memory that decays with time.

\subsection{Maxwell fluid}
Let's first look at a simple model of a viscoelastic fluid called a Maxwell fluid. Consider a shear deformation where adjacent layers of the material are shifted in the same direction along the planes relative to each other. The deformation can be described by the strain $\gamma$, which is approximately the total relative shift divided by the distance between the two layers assuming the displacement is small.

The shear stress $\tau$ for an elastic solid is given by Hooke's law
\begin{align}
\tau_e=G\gamma_e,
\end{align}
where $G$ is the shear modulus of the material. For a Newtonian fluid the constitutive law is
\begin{align}
\tau_v=\mu_p \dgam_v,
\end{align}
where $\mu_p$ is the constant Newtonian viscosity. Graphically we could depict these two elements as a spring and a dampner. Depending on how we connect them, we form a viscoelastic fluid or viscoelastic solid.

\noindent {\bf Combined in series} To combine these two relations to describe the total stress in terms of the total deformation we consider the elastic and viscous elements working in series such that the deformation is distributed ($\gamma=\gamma_e+\gamma_v$) but the stress is the same ($\tau=\tau_e=\tau_v$). Initially, the displacement is taken up by the spring and the dampner, but the displacement of the spring can be redistributed onto the dampner, resulting in the absence of stress at long times. Rearranging these equations we obtain
\begin{align}
\tau+\frac{\mu}{G}\dot{\tau}=\mu\dgam.
\end{align}
This is the constitutive law for a {\it Maxwell fluid}, which is a viscoelastic fluid.

\noindent {\bf Combined in parallel} Combining these two relations in parallel, the stress is now distributed ($\tau=\tau_e+\tau_v$) but the deformation is the same ($\gamma=\gamma_e=\gamma_v$). In this parallel connection, unlike the previous case, the system remains under stress so long as $\gamma \neq 0$. 
Rearranging these equations we obtain
\begin{align}
\tau=G\gamma+\mu\dgam.
\end{align}
This is the constitutive law for a {\it Kelvin-Voigt solid}, which is a viscoelastic solid. In this course, we will be focusing on viscoelastic fluids so we will return to the Maxwell model.

The 1D Maxwell model $\tau+\lambda \dot{\tau}=\mu\dot{\gamma}$, where $\lambda=\mu/G$, can be solved to give
\begin{align}
\frac{d}{dt}\left(e^{t/\lambda}\tau\right)=\frac{\mu\dot{\gamma}}{\lambda}e^{t/\lambda} \quad \Rightarrow \quad \tau(t)=\frac{\mu}{\lambda}\int_{-\infty}^t\dot{\gamma}(t')e^{-(t-t')/\lambda}\,\mathrm{d}t'.
\end{align}
The parameter $\lambda$ is the Maxwell relaxation time. Looking at the expression for the stress response, we see that the stress created by a stepwise deformation relaxes exponentially on a time scale $\lambda$ (viscous-fluid-like property). Whereas, on short times $\tau(t)\sim \mu\gamma(t)/\lambda$ and the material behaves like a solid. For example, consider the stress response to a periodic deformation with frequency $\omega$, where $\gamma=\gamma_0 \sin(\omega t), \, \dot{\gamma}=\gamma_0\omega \cos(\omega t)$. Substituting into the integral expression gives
\begin{align}
\tau(t)&=\frac{\mu\gamma_0\omega}{\lambda}\int_{-\infty}^t\cos(\omega t')e^{-(t-t')/\lambda}\,\mathrm{d}t' =\mu\gamma_0\omega\frac{\cos(\omega t)+\lambda\omega\sin(\omega t)}{1+(\lambda \omega)^2}\\
&=\frac{\mu\dot{\gamma}}{1+(\lambda\omega)^2}+\frac{G(\lambda \omega)^2{\gamma}}{1+(\lambda\omega)^2}=\tilde{\mu}(\omega)\dot{\gamma}+\tilde{G}(\omega)\gamma,
\end{align}
where $\tilde{\mu}(\omega), \, \tilde{G}(\omega)$ are the frequency dependent viscosity and shear modulus. At short times ($\lambda\omega \gg 1$), $\tilde{G}\simeq G$ and the material behaves like a solid. Whereas, at long times  ($\lambda\omega \ll 1$), $\tilde{\mu}\simeq \mu$ and the material behaves as a viscous fluid. The crossover between these two behaves occurs when the time scale of deformation is the same as the time scale of relaxation, $\omega^{-1}\sim\lambda$.

\noindent {\bf Frame invariant?} In three dimensional coordinates, the Maxwell fluid constitutive law can be written as
\begin{align}
\btau+\frac{\mu}{G}\dot{\btau}=\mu\bdgam.
\label{ve1}
\end{align}
As we saw with the generalised Newtonian fluids above, we require the constitutive law to be frame invariant. However, the time derivative of the stress $\btau$ is not frame invariant. 
For example, if we move from a stationary lab frame to one moving with constant velocity $\bu_0$, we have 
\begin{align}
\frac{\partial\tau_{ij}}{\partial t}(\bx +\bu_0t)=\frac{\partial\tau_{ij}}{\partial t}+\bu_0\cdot\grad\tau_{ij}.
\end{align}
But clearly, adding a constant velocity to the fluid should not result in any additional velocity gradients or additional stresses. Hence, the Maxwell fluid is not frame invariant. This is because the time derivatives of individual components of the stress tensor do not form a tensor themselves. To deal with this, the ordinary time derivative needs to be replaced by another derivative.


\newcommand{\ubtau}{\overset{\triangledown}{\btau}}
\subsection{Oldroyd B fluid}
One version of a frame invariant derivative is the {\it upper convected derivative} defined as
\begin{align}
\ubtau_p=\frac{\partial \btau_p}{\partial t}+\bu\cdot\grad\btau_p-(\grad\bu)^T\cdot\btau_p
-\btau_p\cdot(\grad \bu),
\end{align}
which transforms as a covariant tensor. N.B. there are other choices for a frame invariant derivative but we will only consider the upper convected derivative during this course. Rewriting the relationship between stress and strain-rate (\ref{ve1}) using the upper convected derivative gives
\begin{align}
\btau_p+\frac{\mu_p}{G}\ubtau_p=\mu_p\bdgam
\end{align}
to describe the polymeric stress which incorporates both viscous and elastic properties. The relaxation time of the fluid is defined as $\lambda=\mu_p/G$. For a polymeric fluid, there is an additional Newtonian contribution from the solvent viscosity $\mu_s$. Hence, the total deviatoric stress tensor is written as
\begin{align}
\btau=\mu_s\bdgam+\btau_p.
\end{align}
This model is known as the {\it Oldroyd B model}. In the limit of vanishing deformation rates it describes a Newtonian fluid with viscosity $\mu=\mu_s+\mu_p$.  

\vspace{1em}

\hrule
{\bf Oldroyd B Model:}
\begin{align}
&\grad\cdot \bu = 0\\
&\rho\left(\frac{\partial \bu}{\partial t}+\bu\cdot\grad \bu\right)=\grad\cdot\bT\\
&\bT=-p\bI+{\btau}=-p^*\bI+\mu_s\bdgam+G\bA\\
&\overset{\triangledown}{\bA}=
\frac{\partial\bA }{\partial t}+\bu\cdot\grad\bA-(\grad\bu)^T\cdot\bA
-\bA\cdot(\grad \bu)= -\frac{1}{\lambda}(\bA-\bI)
\end{align}
where
\begin{align}
p^*= p+\frac{\mu_p}{\lambda}, \quad G=\frac{\mu_p}{\lambda} \quad \mathrm{and} \quad 
\bA=\frac{1}{G}\left[\btau-\mu_s\bdgam+G\bI\right].
\end{align}

\hrule
Here, we have rewritten the Oldroyd B model in terms of variable $\bA$; a symmetric second rank tensor. This form of the model shows that $\bA$ tends towards its relaxed state $\bA=\bI$ at long times governed by long timescale $\lambda$ at which point the polymer stress $\btau_p$ vanishes. To understand the properties of Oldroyd B fluids let's see how it behaves in some simple fluid flows.

\subsection{Shear flow}
 First consider a shear flow of the form
\begin{align}
\bu=(\dgam y,0,0),
\end{align}
which only depends on $t$  ($\dgam=\dgam(t)$) and $y$ so all variables must also only be functions of $t$ and $y$. By construction this satisfies mass conservation. The momentum equation reduces to
\begin{align}
\rho \frac{\partial \bu}{\partial t}=\grad\cdot \bT,
\end{align}
\newcommand{\cG}{{G}}
where
\begin{align}
\bT=\begin{pmatrix} -p^* & 0\\ 0 & -p^* \end{pmatrix}+
\begin{pmatrix} 0 & \mu_s\dgam\\ \mu_s\dgam & 0 \end{pmatrix}+
\cG\begin{pmatrix} A_{xx} & A_{xy}\\ A_{xy} & A_{yy} \end{pmatrix},
\end{align}
and 
\begin{align}
\frac{\partial}{\partial t}\bA-
\begin{pmatrix} \dgam A_{xy} & \dgam A_{yy}\\ 0 & 0 \end{pmatrix}-
\begin{pmatrix} \dgam A_{xy} & 0\\ \dgam A_{yy} & 0 \end{pmatrix}=
-\frac{1}{\lambda}\begin{pmatrix}A_{xx}-1 & A_{xy}\\ A_{xy} & A_{yy}-1\end{pmatrix}
\end{align}

\noindent{\bf Steady flow}\\
If the strain rate $\dgam$ is constant, the components of $\bA$ must also be independent of $t$. The components then satisfy
\begin{align}
-2\dgam A_{xy}=&\,-\frac{1}{\lambda}(A_{xx}-1)\\
-\dgam A_{yy}=&\,-\frac{1}{\lambda}A_{xy}\\
0=&\,-\frac{1}{\lambda}(A_{yy}-1)\\
-\dgam A_{yy}=&\,-\frac{1}{\lambda}A_{xy}.
\end{align}
Hence the total stress is
\begin{align}
\bT=&\,\begin{pmatrix} -p^*+\cG(1+2\lambda^2\dgam^2)& (\mu_s+\cG\lambda)\dgam
\\ (\mu_s+\cG\lambda)\dgam & -p^*+\cG\end{pmatrix},\\
=&\, \begin{pmatrix}-p+2G\lambda^2\dgam^2 & \mu\dgam \\
\mu\dgam & -p \end{pmatrix}
\end{align}
Compared with the Newtonian equivalent, adding polymers to the solvent causes the viscosity to increase from $\mu_s$ to $\mu=\mu_s+\mu_p$, and there is now a normal stress difference
\begin{align}
T_{xx}-T_{yy}=2G\lambda^2\dgam^2.
\end{align}
This acts in the opposite direction to pressure so is like a tension for stretched polymers along streamlines. This explains the famous rod-climbing experiments, see figure \ref{rod}. A rod in the middle of an Oldroyd B fluid is rotated generating a shear flow in the azimuthal direction. The normal stress difference acts as a tension on the circular streamlines causing them to contract. By mass conservation, the fluid must instead move vertically, hence causing the fluid to climb up the rod.

\begin{figure}
\centering
\includegraphics[width=.7\linewidth]{rod.png}
\caption{\label{rod} Rod climbing experiment. A rod rotates at the centre of a viscoelastic fluid. The normal stresses cause the fluid along streamlines to move inwards, and hence, by mass conservation, are forced to climb the rob. (Adapted from Morozov and Spagnolie, Introduction to Complex fluids.) }
\end{figure}

\newcommand{\dveps}{\dot{\varepsilon}}
\subsection{Extensional flow}
Now let's consider a two dimensional extensional flow of the form
\begin{align}
\bu=(\dveps x,-\dveps y),
\end{align}
where $\dveps$ is constant.
Again, this automatically satisfies mass conservation. The strain-rate and stress tensors are
\begin{align}
\bdgam=&\,\begin{pmatrix} 2\dveps & 0\\ 0 & -2\dveps \end{pmatrix}=
2\grad\bu=2(\grad\bu)^T,\\
\bT=&\, \begin{pmatrix} -p^* & 0\\ 0 & -p^* \end{pmatrix}+
\begin{pmatrix} 2\mu_s\dveps  & 0\\ 0 & -2\mu_s\dveps  \end{pmatrix}+
\cG\begin{pmatrix} A_{xx} & A_{xy}\\ A_{xy} & A_{yy} \end{pmatrix}.
\end{align}
with the components $A_{ij}$ satisfying
\begin{align}
-\dveps\begin{pmatrix}A_{xx} & A_{xy}\\ -A_{xy} & -A_{yy}\end{pmatrix}
-\dveps\begin{pmatrix}A_{xx} & -A_{xy}\\ A_{xy} & -A_{yy}\end{pmatrix}=
-\frac{1}{\lambda}\begin{pmatrix} A_{xx}-1& A_{xy}\\ A_{xy} & A_{yy}-1 \end{pmatrix}.
\end{align}
Rearranging, we find
\begin{align}
A_{xx}=\frac{1}{1-2\lambda\dveps}, \quad A_{xy}=0, \quad A_{yy}=\frac{1}{1+2\lambda\dveps}.
\end{align}
The total stress is then
\begin{align}
\bT=\begin{pmatrix} -p^*+\frac{\cG}{1-4\lambda^2\dveps^2} +2\mu_{ext}\dveps
& 0\\
0 & -p^*+\frac{\cG}{1-4\lambda^2\dveps^2}-2\mu_{ext}\dveps
\end{pmatrix},
\end{align}
where
\begin{align}
\mu_{ext}=\mu_s+\frac{\lambda \cG}{1-4\lambda^2\dveps^2}=
\mu_s+\frac{\mu_p}{1-4\lambda^2\dveps^2}
\end{align}
If we plot this viscosity as a function of the strain rate $\dveps$ (figure \ref{ext}) we see that initially the viscosity increases (hence thickens, or strain hardens). But when $\dveps=1/2\lambda$ the viscosity diverges and then is negative for larger values. This result is clearly unphysical. This happens because the Oldroyd B model is derived using Hooke's law which is infinitely extensible. To get around this a modification, a nonlinear spring law must be used.

\begin{figure}
\centering
\includegraphics[width=.5\linewidth]{ext.png}
\caption{\label{ext}Plot of extensional viscosity for an Oldroyd B fluid against strain rate $\dveps$. At $\dveps=1/2\lambda$ the viscosity diverges and is negative for larger values.}
\end{figure}

\subsection{Weakly non-linear viscoelastic fluids}
A weakly non-linear flow is a situation wherein the flow changes on time scales much longer than the relaxation time, $\lambda$ , and therefore $\lambda$ is in some sense small and can be used as an expansion parameter. It is generally a bad practice to perform a Taylor expansion in a dimensional variable; a better expansion parameter might be $\lambda/T_0$, where $T_0$ is the typical time scale set by the flow.

From the Oldroyd B model we have
\begin{align}
\btau+\lambda\overset{\triangledown}{\btau}=\mu\left(\bdgam+\lambda \frac{\mu_s}{\mu}\overset{\triangledown}{\bdgam}\right),
\label{old1}
\end{align}
where $\mu=\mu_s+\mu_p$. Let $P$ be a characteristic pressure scale, $L$ a characteristic length scale, $L/T_0$ a characteristic velocity scale. The non-dimensional version of equation (\ref{old1}) is
\begin{align}
\btau+\epsilon\overset{\triangledown}{\btau}=\frac{\mu}{PT_0}\left(\bdgam+\epsilon \frac{\mu_s}{\mu}\overset{\triangledown}{\bdgam}\right),
\end{align}
where $\epsilon=\lambda/T_0$. Expanding in this parameter, we may write
\begin{align}
\btau=\btau^{(0)}+\epsilon\btau^{(1)}+O(\epsilon^2).
\end{align}
Substituting this into the Oldroyd B model, we recover a Newtonian constitutive law to leading order, 
\begin{align}
\btau^{(0)}=\frac{\mu}{PT_0}\bdgam,
\quad
\mathrm{and} \quad
\btau^{(1)}=-\overset{\triangledown}{\btau}^{(0)}+\frac{\mu_s}{PT_0}\overset{\triangledown}{\bdgam}=-\frac{\mu_p}{PT_0}\overset{\triangledown}{\bdgam}.
\end{align}
Putting the dimensions back in, we have
\begin{align}
\btau=\mu\bdgam-\lambda\mu_p\overset{\triangledown}{\bdgam}.
\end{align}
This is an example of a {\it second-order fluid}.
\vspace{1em}

\hrule
{\bf Second-order fluid:}
The simplest class of equations for viscoelastic solutions involves the expression of the stress tensor as a sum of all admissible combinations of the velocity gradient tensor. Depending on the highest algebraic power of the velocity gradient tensor involved, they are called the {\it second-order fluid}, {\it third-order fluid}, etc. For example, the deviatoric stress of the second-order fluid is given by
\begin{align}
\btau=\mu\bdgam+b_2\overset{\triangledown}{\bdgam}+b_{11}\bdgam\cdot\bdgam,
\end{align}
where $b_2, \, b_{11}$ are material constants.
\vspace{1em}
\hrule

Returning back to our Oldroyd B model, the weakly non-linear expansion is a second-order fluid with material parameters $b_2=-\lambda\mu_p, \, b_{11}=0$.

\subsection{Rod climbing for a second-order fluid}
For an Oldroyd B fluid we showed that there is a normal stress difference in a shear flow. In this section we will determine the steady free surface of a weakly non-linear second-order fluid caused by a rod of radius $r=a$ rotating at a frequency $\Omega$ in the fluid. Flow is purely in the azimuthal direction so we write ${\bf u}=(0,v(r),0)$ in cylindrical polars.

The flow is weakly non-linear so to leading order, the fluid behaves like a Newtonian fluid where $\dgam_{r\theta}$ is the only non-zero strain-rate component. In terms of the deviatoric stress components, taking $P$ to be a characteristic stress scale, $\tau_{r\theta}(r)\sim O(P)$ and $\tau_{rr}(r), \, \tau_{\theta\theta}(r)\sim O(\epsilon P)$ with pressure $p(r,z)\sim O(P)$. The leading order momentum equations give
\begin{align}
0=\frac{\partial }{\partial r}\left(T_{r\theta}\right)+\frac{2}{r}T_{r\theta}=\frac{1}{r^2}\frac{\partial }{\partial r}\left(r^2T_{r\theta}\right), \quad 0=\frac{\partial T_{zz}}{\partial z}-\rho g.
\end{align}
We can use these to solve for the azimuthal component of the velocity. The leading order r-momentum equation gives $\partial p/\partial r=0$. The r-momentum equation at order $O(\lambda/T_0)$ can be used to calculate the higher order components of the stress
\begin{align}
0=\frac{\partial T_{rr}}{\partial r}+\frac{T_{rr}-T_{\theta\theta}}{r}.
\end{align}
For the leading order balance, the fluid has a Newtonian constitutive law:
\begin{align}
\dgam_{r\theta}=r\frac{d}{dr}\left(\frac{v}{r}\right), \, T_{r\theta}=\mu\dgam_{r\theta} \quad \Rightarrow \quad T_{r\theta}=\frac{A}{r^2}=\frac{d}{dr}\left(\frac{v}{r}\right)
\end{align}
Integrating and applying boundary conditions $v(a)=\Omega a, \, v\rightarrow 0$ as $r\rightarrow \infty$, gives
\begin{align}
v=\frac{\Omega a^2}{r}, \quad \dgam_{r\theta}=r\frac{d}{dr}\left(\frac{v}{r}\right)=-\frac{2\Omega a^2}{r^2}.
\end{align}
The vertical component of the Stokes equation gives a hydrostatic balance
\begin{align}
0=\frac{\partial T_{zz}}{\partial z}-\rho g\quad \Rightarrow \quad p=\rho g(h(r)-z).
\end{align}

We now consider the second-order contribution to the stress. Using the second-order fluid model, we have
\begin{align}
T_{r\theta}=\mu\dgam_{r\theta}, \quad T_{rr}=-p+b_{11}\dgam_{r\theta}^2, \quad T_{\theta\theta}=-p-2b_2\dgam_{r\theta}^2+b_{11}\dgam_{r\theta}^2, \quad T_{zz}=-p.
\end{align}
since $(\overset{\triangledown}{\bdgam})_{rr}=0, \, (\overset{\triangledown}{\bdgam})_{\theta\theta}=-2\dgam_{r\theta}^2$. Substituting into the r-momentum balance gives
\begin{align}
&\frac{\partial T_{rr}}{\partial r}+\frac{2b_2\dgam_{r\theta}^2}{r}=0 \quad \Rightarrow \quad -p+b_{11}\dgam_{r\theta}^2=\frac{b_2}{2}\dgam_{r\theta}^2+f(z)\\
&-\rho g(h(r)-z)+(b_{11}-b_2/2)\dgam_{r\theta}^2=f(z).
\end{align}
Choosing $f(z)=\rho g z$, we arrive at
\begin{align}
h(r)=\frac{1}{\rho g}\left(b_{11}-\frac{b_2}{2}\right)\dgam_{r\theta}^2.
\end{align}
Referring back to the material parameters for the Oldroyd B model, we see the surface has height
\begin{align}
h(r)=\frac{2\lambda \mu_p \Omega^2 a^4}{\rho g r^4}.
\end{align}
Hence, for an Oldroyd B model, rod climbing occurs.


\subsection{Words of caution}
\begin{enumerate}
\item {\bf The linear Maxwell model is not objective}\\
The model is not frame invariant so none of the conclusions drawn from studying the model can be guaranteed to be physical.

\item {\bf Time-dependent flows in weakly non-linear viscoelastic fluids are unstable}\\
Consider the second-order fluid model
\begin{align}
\btau=\mu\bdgam+b_2\overset{\triangledown}{\bdgam}+b_{11}\bdgam\cdot\bdgam
\end{align}
in a two-dimensional shear flow ${\bf u}=(u(y,t),0)$ in a channel $0\leq y\leq h$. From momentum balance we have
\begin{align}
\rho\frac{\partial u}{\partial t}=\frac{\partial T_{xy}}{\partial y}\equiv\frac{\partial \tau_{xy}}{\partial y}.
\end{align}
From the constitutive law, the deviatoric stress $\tau_{xy}$ is 
\begin{align}
\tau_{xy}=\mu\frac{\partial u}{\partial y}+b_2\frac{\partial}{\partial t}\frac{\partial u}{\partial y}.
\end{align}
These two can be combined to give
\begin{align}
\rho\frac{\partial u}{\partial t}=\mu\frac{\partial^2 u}{\partial y^2}+b_2\frac{\partial}{\partial t}\frac{\partial^2 u}{\partial y^2}.
\end{align}
Taking no-slip boundary conditions at the walls of a channel located at $y = 0$ and $y = h$, we write the flow velocity as a Fourier sine series:
\begin{align}
u(y,t)=\Sigma_{m=1}^{\infty}u_me^{\alpha_m t}\sin\left(\frac{m\pi y}{h}\right).
\end{align}
Substituting this into the above equation and rearranging, we find growth rates $\alpha_m$
\begin{align}
\alpha_m=-\frac{\mu}{b_2+\frac{\rho h^2}{(m\pi)^2}}.
\end{align}
For polymer solutions $b_2$ is typically negative (recall its value based on the Oldroyd B model, $b_2 = -\lambda \mu_p$), so that $\alpha_m$ is positive for sufficiently large $m$. This implies that a steady shear flow of a second-order fluid is unstable to short-wavelength perturbations and cannot be realized. In the case of negligible inertia, achieved in the above by setting $\rho = 0$, this predicts that all Fourier modes are unstable. The implications of this result are profound: it shows that an approximation of slow flows, or, in other words, Taylor expansions of the stress in terms of the relaxation time cannot be used in time-dependent flows where any shear component would result locally in a linear instability and exponential growth of the stress. Hence, the second-order fluid and similar approximations should generally not be used to study time-dependent flows!
\end{enumerate}

