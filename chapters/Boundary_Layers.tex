\chapter{Boundary layers}
Boundary layers are regions of space adjacent to boundaries of fluid domains where affects of viscosity may not be ignored no matter how small the coefficient of viscosity may be. The characteristics of and the dynamics within boundary layers are of interest especially when the coefficient of viscosity is vanishlingly small. The example of the boundary layer next to a flat plate aligned with the flow, the topic of this chapter, illustrates the essential features.

\section{Flow next to a flat plate}
Consider a uniform flow of speed $U$ along the $x$-axis being incident on a flat plate of infinitesimal thickness and length $L$ aligned with the flow, as schematically shown in Figure~\ref{fig:BLSchematic}. The plate is oriented perpendicular to the plane of paper. The fluid has viscosity $\mu$ and density $\rho$. We ignore gravity in this example. The potential flow $\bu = U\e_x$ with a uniform pressure satisfies mass and momentum conservation, and the no-penetration condition on the flat plate, but not the no-slip condition. For an inviscid fluid, this is a perfectly valid flow, but not for a viscous fluid, no matter how small the viscosity coefficient. In this manner, this example offers the opportunity to study how the imposition of the no-slip condition modifies the simplest of potential flows in the presence of the slightest of viscosity. Hence, we choose a vanishingly small coefficient of viscosity $\mu$.
\begin{figure}[h]
\centerline{\includegraphics{BLSchematic}}
\caption{Schematic of a flow past a flat plate aligned with the flow. Here the thickness of the boundary layer is denoted as $\delta$.}
\label{fig:BLSchematic}
\end{figure}

\subsection{Qualitative description of the flow} \label{subsec:qual}

\iflatexml
\begin{figure}[ht]
    \includegraphics[width=\textwidth]{BLVelocity_Panels}
    \caption{Profiles of $u$ versus $y$ taken at 7 distances from the leading edge of the plate. (a-l) Columns correspond to constant $U$ listed at the top of each columns and rows to constant $\mu$ listed to the left.}
    \label{fig:BLVelocity_Panels}
\end{figure}
\else
\afterpage{\clearpage}
\begin{sidewaysfigure}[ht]
    \includegraphics[width=\textheight]{BLVelocity_Panels}
    \caption{Profiles of $u$ versus $y$ taken at 7 distances from the leading edge of the plate. (a-l) Columns correspond to constant $U$ listed at the top of each columns and rows to constant $\mu$ listed to the left.}
    \label{fig:BLVelocity_Panels}
\end{sidewaysfigure}
\fi

Let us first develop a picture of the dynamics of this flow in our minds. The approach we take is to make various assumptions about the flow now and verify them later. This flow is driven by the inertia of the fluid in the freestream. The fluid particles away from the plate start out with speed $U$ and will continue to do so unless an unbalanced force makes them decelerate. The action of the no-slip boundary condition and the shear stress that develops in the fluid is expected to slow down the fluid in the vicinity of the flat plate. A typical profile $u$ versus $y$ is shown in Figure~\ref{fig:BLSchematic}.
The boundary layer, the region where the flow speed appreciably falls blow the freestream value $U$, is expected to be of thickness $\delta$ that is also vanishingly small with the coefficient of viscosity. Outside this layer, we expect the flow to be approximately uniform, and certainly described by potential flow. 
A natural consequence of the slowing down of the flow in the boundary layer is that the fluid upstream that is incident just above the plate now needs to be deflected away from the plate. Therefore, a $y$ components of velocity also naturally appears in the flow. However, because of the thinness of the boundary layer, the amount of deflection is weak and the $y$ component is expected to be smaller in magnitude than the $x$ component.

A computational solution of Navier-Stokes equations is used to assist with the qualitative description of the flow. The commercial finite elements software COMSOL is used to obtain the solution. The particular case considered is for $L = 5$ cm, $\rho = 1$ kg/m$^3$ and several values for $\mu$ and $U$ (listed in Figure~\ref{fig:BLVelocity_Panels}. The plate in this case has a finite thickness equal to 1/64 the length. The computational box is chosen to be 10 cm $\times$ 10 cm. (See the LectureCapture video from March 6, 2020, for more details.)

To obtain some insight into the thickness of the boundary layer, Figure~\ref{fig:BLVelocity_Panels} shows the computed profiles of velocity for different values of $U$ and $\nu$. It is evident that increasing $U$ decreases $\delta$, while increasing $\mu$ increases it. The growth of the boundary layer with $x$, the distance form the leading edge, is also evident. These are the features we seek to explain in the subsequent analysis. 

A scaling estimate for $\delta$ may be made in the folowing way. A fluid particle starting from the leading edge of the plate traveling with speed $U$ takes time $t=x/U$ to traverse the distance $x$. Perpendicular to the direction of travel, viscosity influences a distance $\delta = \sqrt{\nu t}$ in time $t$, where $\nu=\mu/\rho$ is the kinematic viscosity. In this case, this implies $\delta = \sqrt{\nu x/U}$. Miraculously, replotting a rescaled version of the data shown in Figure~\ref{fig:BLVelocity_Panels} collapses each one of those profiles of $u$ versus $y$ very close to a universal curves. Figure~\ref{fig:BLVelocity_Collapse} demonstrates this collapse where $y$ is rescaled using $\delta(x)$ and $u$ with $U$. To explain this miraculous collapse, to determine the universal shape the velocity collapses to, and to better understand the underlying fluid dynamics, we proceed with certain simplifying assumptions about the flow.

It can be seen from this estimate that the boundary layer becomes vanishingly thin as $\nu$ approaches zero. The slope of the growing boundary layer thickness is proportional to $\delta/x$, and therefore, we expect the $y$ component of velocity to be of the magnitude $U\delta/x$. Subsequent analysis proceeds under the assumption that $\delta$ is much smaller than $L$ in the following analysis to derive the so-called Prandtl boundary-layer equations. 

\begin{figure}
\centerline{\includegraphics{BLVelocity_Collapse}}
\caption{Collapse of the profiles of $u$ versus $y$ when rescaled. Each of the curves in Figure~\ref{fig:BLVelocity_Panels} is plotted as the coloured curves in this figure. Here $\delta$ is from \eqref{eqn:delta}. The dashed black curve corresponds to the solution of (\ref{eqn:blasius}-\ref{eqn:blasbc}).}
\label{fig:BLVelocity_Collapse}
\end{figure}


\subsection{Derivation of the Prandtl boundary layer equations}\label{subsec:prandtl}
In the limit of small viscosity, and especially due to the boundary layer being thinner than the length of the plate, the governing Navier-Stokes equations simplify to the simpler Prandtl boundary layer equations. To derive them, let us start with the steady two-dimensional Navier-Stokes equations
\begin{subequations}
\begin{align}
 \pd{u}{x} + \pd{v}{y} &= 0, \label{eqn:nsmass} \\
 \rho\left( u \pd{u}{x} + v \pd{u}{y} \right) &= -\pd{p}{x} + \mu \left(\pdd{u}{x} + \pdd{u}{y}\right), \label{eqn:nsxmom} \\
 \rho\left( u \pd{v}{x} + v \pd{v}{y} \right) &= -\pd{p}{y} + \mu \left(\pdd{v}{x} + \pdd{v}{y}\right). \label{eqn:nsymom}
\end{align}
\label{eqn:ns}
\end{subequations}
Noting the reflection symmetry of the flow around $y=0$, we only solve for $u$, $v$ and $p$ for $y>=0$.
These equations are to be solved with the boundary conditions
\begin{subequations}
\begin{align}
 u(x\geq 0,y=0) &= 0, \label{eqn:nsbcnoslip} \\
 v(x\geq 0,y=0) &= 0, \label{eqn:nsbcnopen} \\
 u(x, y \to \infty) &= U. \label{eqn:nsbcfreestream}
\end{align}
\label{eqn:nsbc}
\end{subequations}

\subsubsection{Scaling estimate of the size of terms in Navier-Stokes equations\label{subsec:scaling}}
The first step is to develop a scaling estimate for each of the terms in these equations. Here our objective is to identify how each term in \eqref{eqn:ns} scales with the parameters of the problem, viz., $\rho$, $\mu$, $U$ and $L$, and then balance the magnitudes of the dominant terms.

\begin{table}
\bgroup
\renewcommand{\arraystretch}{3.0}%
\begin{tabular}{|c|c|c|c|c|c|}
\hline
{\bf Equation} & \multicolumn{4}{c}{\bf Terms and their magnitudes} & \\ \hline
Mass conservation \eqref{eqn:nsmass}         & $\pd{u}{x} \sim \dfrac{U}{L}$              & $\pd{v}{y} \sim \dfrac{V}{\delta}$ & & & \\ \hline
$x$-momentum conservation \eqref{eqn:nsxmom} & $\rho u\pd{u}{x} \sim \dfrac{\rho U^2}{L}$ & $\rho v \pd{u}{y} \sim \dfrac{\rho UV}{\delta}$ & $\pd{p}{x} \sim \dfrac{P}{L}$ & $\mu \pdd{u}{x} \sim \dfrac{\mu U}{L^2}$ & $\mu \pdd{u}{y} \sim \dfrac{\mu U}{\delta^2}$ \\ \hline
$y$-momentum conservation \eqref{eqn:nsymom} & $\rho u\pd{v}{x} \sim \dfrac{\rho UV}{L}$ & $\rho v \pd{v}{y} \sim \dfrac{\rho V^2}{\delta}$ & $\pd{p}{y} \sim \dfrac{P}{\delta}$ & $\mu \pdd{v}{x} \sim \dfrac{\mu V}{L^2}$ & $\mu \pdd{v}{y} \sim \dfrac{\mu V}{\delta^2}$ \\ \hline
(magnitudes of \eqref{eqn:nsymom} simplified) & $\dfrac{\rho U^2}{L} \dfrac{1}{\sqrt\Rey}$ & $\dfrac{\rho U^2}{L} \dfrac{1}{\sqrt\Rey}$ & $\dfrac{\rho U^2}{L} \sqrt\Rey$ & $\dfrac{\rho U^2}{L} \dfrac{1}{\sqrt{\Rey^{3}}}$ & $\dfrac{\rho U^2}{L} \dfrac{1}{\sqrt\Rey}$ \\ \hline
\end{tabular}
\egroup
\caption{Scaling estimate of the magnitude of terms in \eqref{eqn:ns}.}
\label{tab:Estimates}
\end{table}

Each dependent and independent variable is assigned a constant magnitude in terms of the parameters governing the problem. For example, the magnitude of $u$ is taken to be $U$, expressed as $u\sim U$. Similarly, the scale for $x$ is taken to be $L$, i.e. $x\sim L$. We do not yet know the magnitudes for $v$, $y$ and $p$, so we define symbols $V$, $\delta$ and $P$, respectively, to represent them. The magnitudes of partial derivatives are estimated using ratios of the variables in the derivative, e.g., $\pd{u}{x} \sim \dfrac{U}{L}$. Our task is to express these undefined symbols in terms of the known parameters using dominant balances.

As an illustration, consider the mass conservation equation \eqref{eqn:nsmass}. The magnitude of the two terms in this equation are presented in the second row of Table~\ref{tab:Estimates}. Balancing the magnitudes of the two terms yields the magnitude for $v$ as
\begin{align}
 \dfrac{U}{L} = \dfrac{V}{\delta} \qquad \Longrightarrow \qquad V = \dfrac{U\delta}{L}.
 \label{eqn:vscale}
\end{align}
Note that this is consistent with our estimates in the simple picture in \S\ref{subsec:qual}.

Table~\ref{tab:Estimates} also shows the magnitude estimates for the momentum conservation equations in \eqref{eqn:ns}. Using \eqref{eqn:vscale}, it is evident that in \eqref{eqn:nsxmom} 
\begin{align}
 \dfrac{\rho U^2}{L} = \dfrac{\rho UV}{\delta},
\end{align}
implying that both the terms $\rho u \pd{u}{x}$ and $\rho v \pd{u}{y}$ are of equal magnitude. Furthermore, given that the pressure field is established to enforce incompressibility, its magnitude is set up to balance the strongest term in the equations. This implies that the $\rho u \pd{u}{x}$ is of a magnitude comparable to $\pd{p}{x}$, or
\begin{align}
 \dfrac{P}{L} = \dfrac{\rho U^2}{L} \qquad \Longrightarrow \qquad P = \rho U^2.
 \label{eqn:pscale}
\end{align}
Between the two viscous terms, $\mu \pdd{u}{x}$ and $\mu \pdd{u}{y}$, which scale as $\mu U/L^2$ and $\mu U/\delta^2$, respectively, because $\delta \ll L$ it is evident that, $\mu \pdd{u}{x} \ll \mu \pdd{u}{y}$. Thus, the term $\mu \pdd{u}{x}$ is perhaps negligible compared to at least one other term in \eqref{eqn:nsxmom}. Neglecting the $\mu \pdd{u}{y}$ also will render the dominant balance equivalent to that of an inviscid fluid, which is contrary to our original intention of studying the influence of viscosity. Therefore, balancing the dominant of the two viscous terms $\mu \pdd{u}{y}$ with the inertial terms yields
\begin{align}
 \dfrac{\rho U^2}{L} = \dfrac{\mu U}{\delta^2} \qquad \Longrightarrow \qquad \delta = \sqrt{\dfrac{\nu L}{U}}.
\label{eqn:deltascale}
\end{align}
This expression is eerily similar to one derived in \S\ref{subsec:qual}, except that $x$ is replaced by $L$. We can now verify our assumption 
\begin{align}
 \dfrac{\delta}{L} \ll 1 \qquad \Longleftrightarrow \qquad \sqrt{\dfrac{\nu}{UL}} = \Rey^{-1/2} \ll 1 \qquad \Longleftrightarrow \qquad \Rey \gg 1,
\end{align}
where we defne the Reynolds number $\Rey = UL/\nu$.

We now turn our attention to \eqref{eqn:nsymom}. Substituting $V$ from \eqref{eqn:vscale} and $P$ from \eqref{eqn:pscale} into \eqref{eqn:nsymom} yields the magnitude of every term in \eqref{eqn:nsymom}. In light of the assumption $\delta \ll L$ yields the most dominant term in that equation to be $\pd{p}{y}$, which scales as $\dfrac{\rho U^2}{L} \times \sqrt\Rey$, while all the other terms are negligible in comparison. See Table~\ref{tab:Estimates} for details.

With these estimates, \eqref{eqn:ns} simplify to
\begin{subequations}
\begin{align}
 \pd{u}{x} + \pd{v}{y} &= 0, \label{eqn:prmass} \\
 \rho\left( u \pd{u}{x} + v \pd{u}{y} \right) &= -\pd{p}{x} + \mu \pdd{u}{y}, \label{eqn:prxmom} \\
 0 &= -\pd{p}{y}, \label{eqn:prymom}
\end{align}
\label{eqn:prandtl}
\end{subequations}
which are subject to boundary conditions given by \eqref{eqn:nsbc}. The set of equations \eqref{eqn:prandtl} are known as the Prandtl boundary-layer equations after Ludwig Prandtl, who first identified the presence of boundary layers in high Reynolds number flow of viscous fluids, and the approximation leading to them is called the Prandtl boundary-layer approximation.

For flow past a flat plate, the flow is uniform outside the boundary layer, and consequently the pressure is uniform there. Combined with \eqref{eqn:prymom}, this implies that the pressure is uniform everywhere and $\pd{p}{x} = 0$. Thus, for the case of flow past a flat plate, \eqref{eqn:prandtl} simplify further to
\begin{subequations}
\begin{align}
 \pd{u}{x} + \pd{v}{y} &= 0, \label{eqn:fpprmass} \\
 u \pd{u}{x} + v \pd{u}{y} &= \nu \pdd{u}{y}. \label{eqn:fpprxmom}
\end{align}
\label{eqn:flatplateprandtl}
\end{subequations}

\subsection{Self-similar flow past a semi-infinite flat plate}
In a region closer to the leading edge of the plate, say a distance $x$, which is much larger than $\nu/U$ but much smaller than the plate length, $L$, a self-similar structure emerges for the flow. We expose this structure in this section.

\subsubsection{Scale invariance}
The scale invariance of the geometry of the flow past a long plate, approximated as semi-infinite, is evident. Upon zooming out, the plate appears semi-infinite and the uniform flow remains uniform. The governing dynamics in the boundary layer are also scale invariant. To see this, rescale the variables in \eqref{eqn:flatplateprandtl} and \eqref{eqn:nsbc} as
\begin{align}
 u = \alpha \tr{u}, \quad v = \beta \tr{v}, \quad x = \gamma \tr{x} \quad \text{and} \quad y = \delta \tr{y},
 \label{eqn:scalingtr}
\end{align}
where $\alpha$, $\beta$, $\gamma$ and $\delta$ are positive transformation factors.
The rescaled equations \eqref{eqn:flatplateprandtl} and \eqref{eqn:nsbc} are
\begin{subequations}
\begin{align}
 \dfrac{\alpha}{\gamma} \pd{\tr{u}}{\tr{x}} + \dfrac{\beta}{\delta} \pd{\tr{v}}{\tr{y}} &= 0, \label{eqn:fptrmass} \\
 \dfrac{\alpha^2}{\gamma} \tr{u} \pd{\tr{u}}{\tr{x}} + \dfrac{\alpha\beta}{\delta} \tr{v} \pd{\tr{u}}{\tr{y}} &= \dfrac{\alpha}{\delta^2} \nu \pdd{\tr{u}}{\tr{y}}, \label{eqn:fptrxmom}
\end{align}
\label{eqn:trprandtl}
\end{subequations}
and
\begin{subequations}
\begin{align}
 &\alpha \tr{u}(\alpha \tr{x}\geq 0,\delta \tr{y}=0) = 0,    & &\Longrightarrow& & \tr{u}(\tr{x}\geq 0, \tr{y}=0) = 0 & \label{eqn:trbcnoslip} \\
 &\beta \tr{v}(\alpha\tr{x}\geq 0, \delta \tr{y}=0) = 0,     & &\Longrightarrow& & \tr{v}(\tr{x}\geq 0, \tr{y}=0) = 0 & \label{eqn:trbcnopen} \\
 &\alpha \tr{u}(\alpha \tr{x}, \delta \tr{y} \to \infty) = U.& &\Longrightarrow& & \tr{u}(\tr{x}, \tr{y} \to \infty) = \dfrac{U}{\alpha} & \label{eqn:trbcfreestream}
\end{align}
\label{eqn:trbc}
\end{subequations}
Equation \eqref{eqn:trprandtl} and \eqref{eqn:trbc} are identical to \eqref{eqn:flatplateprandtl} and \eqref{eqn:nsbc} if
\begin{align}
\dfrac{\beta}{\delta} = \dfrac{\alpha}{\gamma}, \quad \dfrac{\alpha^2}{\gamma} = \dfrac{\alpha}{\delta^2}, \quad \text{and} \quad \alpha = 1.
\end{align}
These relations between the scaling factors are satisfied if $\alpha = 1$, $\delta = \sqrt{\gamma}$ and $\beta = 1/\sqrt{\gamma}$, irrespective of the value of $\gamma$.
This demonstrates the invariance of the governing equations, and thereby the flow, to the (anisotropic) scaling of the independent and dependent variables. 

The scale invariance implies that the flow velocity at points related by the transformation is also related. In particular,
If the functional form of $u(x,y)$ is given by $u(x,y) = F(x,y)$, then an indetical form descrives the velocity in the transformed coordinates, i.e., $\tr{u}(\tr{x}, \tr{y}) = F(\tr{x}, \tr{y})$. Given the relation between $u$ and $\tr{u}$ from \eqref{eqn:scalingtr},
\begin{align}
 F(x,y) = \alpha F(\tr{x}, \tr{y}) = \alpha F\left(\dfrac{x}{\gamma}, \dfrac{y}{\delta} \right).
\end{align}
Expressing $\alpha$ and $\delta$ in terms of $\gamma$ and $F$ in terms of $u$ yields
\begin{align}
% u(x,y) = \tr{u}( \tr{x}, \tr{y}) \quad \Longrightarrow \quad u(x,y) = \dfrac{1}{\alpha} u\left(\dfrac{x}{\gamma}, \dfrac{y}{\delta} \right) \quad \text{or} \quad 
u(x,y) = u\left(\dfrac{x}{\gamma}, \dfrac{y}{\sqrt{\gamma}} \right).
\end{align}
Choosing $\gamma = x$ reduces this relation to a form useful for our interpretation as
\begin{align}
 u(x,y) = u\left(1, \dfrac{y}{\sqrt{x}} \right).
 \label{eqn:usimilar}
\end{align}
This equation states that the profile of $u$ versus $y$ at any location $x$ is related to that at $x=1$ (arbitrary units) by a simple rescaling of $y$ by $\sqrt{x}$. Similarly,
\begin{align}
v(x,y) = \dfrac{1}{\sqrt{x}} v\left(1, \dfrac{y}{\sqrt{x}} \right).
\end{align}
In this way, we expect the velocity profile to not be independently dependent on $x$ and $y$, but merely on the combination $y/\sqrt{x}$ alone.

\subsubsection{Determination of the Blasius self-similar profile}
Once the possibility of self-similarity is established, the following approach can be used to simplify the governing equations. First, a precursory method incorporates the dependence of the dimensional parameters into the scaling invariance. At this stage, we simply replace derivatives in \eqref{eqn:flatplateprandtl} with ratios, and replace the dependent and independent variables with the scaling estimates in terms of known problem parameters, very similar to scaling estimates in \S\ref{subsec:scaling}. In this analysis, armed by the knowledge of scale invariance, we recognize that $x$ itself represents the scale for the independent variable $x$, and $\delta(x)$ for $y$. The scales for $u$ and $v$ are $U$ and $U\delta(x)/x$, respectively, in view of \eqref{eqn:fpprmass}. The dominant balance between terms in \eqref{eqn:fpprxmom} is 
\begin{align}
 \dfrac{U^2}{x} = 2 \nu \dfrac{U}{\delta(x)^2} \qquad \Longrightarrow \qquad \delta(x) = \sqrt{\dfrac{2 \nu x}{U}},
 \label{eqn:delta}
\end{align}
in agreement with the intuition presented in \S\ref{subsec:qual}, except for the factor of 2. (The explanation for this factor being an algebraic convenience will appear soon.) This approach also explains the appearance of $L$ in the expression for $\delta$ in \eqref{eqn:deltascale} instead of $x$ as in \eqref{eqn:delta}. Because of the scale-invariance, the plate length $L$ is not the appropriate scale for $x$, but $x$ itself is. Apart from this difference, the balance of terms that led to \eqref{eqn:delta} and \eqref{eqn:deltascale} are identical. Equation \eqref{eqn:delta} now incorporates the dimensional dependence on the parameters $\nu$ and $U$.

We proceed to determine the self-similar flow profile by incorporating the results \eqref{eqn:delta} and \eqref{eqn:usimilar} by asserting that
\begin{align}
 u = U f'(\xi), \quad \text{where} \quad \xi = \dfrac{y}{\delta(x)}.
 \label{eqn:ansatzu}
\end{align}
Here the variables $\xi$ and $f(\xi)$ are dimensionless and $'$ denotes differentiation with respect to the argument. There exist multiple equivalent formulations, which represent the same flow, from which we choose one.
To facilitate the subsequent analysis, note that
\begin{align}
\dfrac{d\delta}{dx} = \sqrt{\dfrac{\nu}{2 U x}} = \dfrac{\delta}{2x} \quad \text{so} \quad \pd{\xi}{x} = -\dfrac{y}{\delta^2} \delta'(x) = - \dfrac{\xi}{2x} \quad \text{and} \quad \pd{\xi}{y} = \dfrac{1}{\delta}.
\label{eqn:key}
\end{align}
Using \eqref{eqn:key},
\begin{subequations}
\begin{align}
 \pd{u}{x} &= U f''(\xi) \pd{\xi}{x} = -\dfrac{U}{2x} \xi f''(\xi), \label{eqn:keyux} \\
 \pd{u}{y} &= U f''(\xi) \pd{\xi}{y} = \dfrac{U}{\delta} f''(\xi), \label{eqn:keyuy} \\
 \pdd{u}{y} &= \dfrac{U}{\delta^2} f'''(\xi), \label{eqn:keyuyy} \\
 \pd{v}{y} & = \dfrac{1}{\delta} \pd{v}{\xi}. \label{eqn:keyvy}
\end{align}
\label{eqn:list}
\end{subequations}
Substitution into \eqref{eqn:fpprmass} yields
\begin{align}
 \pd{v}{\xi} = U \dfrac{\delta}{2x} \xi f''(\xi), \qquad \Longrightarrow \qquad v = U \dfrac{\delta}{2x} \left( \xi f'(\xi) - f(\xi) \right).
 \label{eqn:ansatzv}
\end{align}
Substituting \eqref{eqn:ansatzu} and \eqref{eqn:ansatzv} in \eqref{eqn:fpprxmom} yields
\begin{align}
\dfrac{U^2}{2x} \left( -\xi f'(\xi) f''(\xi) + \left( \xi f'(\xi) - f(\xi) \right) f''(\xi)\right) = \nu \dfrac{U}{\delta^2} f'''(\xi).
\label{eqn:preblasius}
\end{align}
This equation reveals the utility of the dominant balance using scaling. Note that in \eqref{eqn:delta} we balanced the terms $U^2/x$ and $\nu U/\delta^2$, which are also the dimensional pre-factors of the terms in \eqref{eqn:preblasius}. The additional factor of 2 in \eqref{eqn:delta} is simply to account for the factor of 2 that arises in \eqref{eqn:preblasius} -- this factor is not necessary for deriving the self-similar flow profile. The essential reason underlying the utility of the method to derive \eqref{eqn:delta} is that, according to the chain rule applied in \eqref{eqn:key} and \eqref{eqn:list}, differentiation with respect to $x$ yields a dimensional factor of $1/x$, and with respect to $y$ yields $1/\delta$. These were precisely the rules we used to derive \eqref{eqn:delta}.

In view of \eqref{eqn:delta}, equation \eqref{eqn:preblasius} simplifies to
\begin{align}
 f'''(\xi) + f(\xi) f''(\xi) = 0.
 \label{eqn:blasius}
\end{align}
The boundary conditions \eqref{eqn:nsbc} simplify to
\begin{subequations}
\begin{align}
 f'(\xi=0) = 0, \label{eqn:blasnoslip} \\
 f(\xi=0)  = 0, \label{eqn:blasnopenetration} \\
 f'(\xi \to \infty) = 1. \label{eqn:blasfarfield}
\end{align}
\label{eqn:blasbc}
\end{subequations}
Equations (\ref{eqn:blasius}-\ref{eqn:blasbc}) are called the Blasius equations for the self-similar profile. They constitute an ordinary differential boundary value problem, which cannot be solved in closed form analytically but can be easily solved numerically. The solution is shown in Figure~\ref{fig:BLVelocity_Collapse} as the dashed curve, which agrees very well with the numerically computed profiles. 

\subsection{Drag on the plate}
With the complete flow field described analytically, we now return to our original motivation of estimating the drag on the plate because of viscous shear stress. The uniform potential flow violates the no-slip boundary condition and, in the process, also fails to account for the drag on the plate. The boundary layer approximation retains the no-slip condition, and thus might be able to explain the drag on the plate.

The drag on the upper surface of the plate is calculated by integrating the tangential component of the shear stress. The components of the total (i.e., Cauchy) stress tensor are
\begin{align}
 \bT = -p \begin{bmatrix} 1 & 0 \\ 0 & 1 \end{bmatrix} +  \mu \begin{bmatrix} 2\pd{u}{x} &  \pd{u}{y} + \pd{v}{x} \\ \pd{u}{y} + \pd{v}{x} & 2 \pd{v}{y} \end{bmatrix}.
\end{align}
The tangential component of the traction on the plate is
\begin{align}
\e_x \cdot \bT \cdot \e_y = \mu \left(\pd{u}{y} + \pd{v}{x} \right) \approx \mu \pd{u}{y}.
\label{eqn:tgttraction}
\end{align}
Note that the component of stress due to pressure does not exert a tangential force on the plate. And using scaling estimates developed in \S\ref{subsec:prandtl}, amongst the terms in \eqref{eqn:tgttraction}, $\pd{u}{y}$ scales as $U/\delta$ and dominates over $\pd{v}{x}$, which scales as $V/L = U \delta/L^2$. Therefore, to the level of approximation we have retained, the term $\pd{v}{x}$ may be neglected.

The total force, $D$, per unit length out of the plane of the paper now may be estimated using \eqref{eqn:keyuy} as
\begin{align}
 D = \int_0^L \mu \pd{u}{y}|_{y=0}~dx = \int_0^L \dfrac{\mu U}{\delta(x)} f''(\xi=0)~dx = \sqrt{\dfrac{\mu^2U^3}{2\nu}} f''(\xi) \int_0^L \dfrac{dx}{\sqrt{x}} = \sqrt{2\mu\rho U^3 L}f''(0).
\end{align}
The value of $f''(0)$ from the solution of (\ref{eqn:blasius}-\ref{eqn:blasbc}) is 0.4696. Substituting this value yields an expression for the drag on a flat plate align with a flow at high Reynolds number as
\begin{align}
 D = 0.664 \sqrt{\rho \mu U^3 L}.
 \label{eqn:drag}
\end{align}
A comparison of the results of \eqref{eqn:drag} with that of the complete solution computed using COMSOL is shown in Table~\ref{tab:dragcomparison}. The simple expression in \eqref{eqn:drag} agrees well with the results of the full computational solution. Reasons for the remaining slight disagreement are the finite thickness of the plate in the COMSOL simulation and the approximations made in the boundary layer theory.

\begin{table}
\begin{minipage}{0.48\textwidth}
\begin{center}
\begin{tabular}{p{1.5cm}ccc}
$\mu$ (Pa s) / $U$ (m/s) & $\mu = 10^{-5}$ & $3\times 10^{-5}$ & $5\times 10^{-5}$ \\
$U=1.0$ & 4.13 & 7.85 & 10.6 \\
$U=1.3$ & 5.99 & 11.4 & 15.4 \\
$U=1.6$ & 8.04 & 15.3 & 20.6 \\
$U=1.9$ & 10.3 & 19.5 & 26.3
\end{tabular}
\end{center}
\end{minipage}
\begin{minipage}{0.48\textwidth}
\begin{center}
\begin{tabular}{p{1.5cm}ccc}
$\mu$ (Pa s) / $U$ (m/s) & $\mu = 10^{-5}$ & $3\times 10^{-5}$ & $5\times 10^{-5}$ \\
$U=1.0$ & 4.70 & 8.13 & 10.5 \\
$U=1.3$ & 6.96 & 12.1 & 15.6 \\
$U=1.6$ & 9.50 & 16.5 & 21.2 \\
$U=1.9$ & 12.3 & 21.3 & 27.5
\end{tabular}
\end{center}
\end{minipage}
\caption{Drag on the plate computed using COMSOL (left) compared with the result of the boundary layer approximation \eqref{eqn:drag} (right).}
\label{tab:dragcomparison}
\end{table}

According to \eqref{eqn:drag}, longer plates experience greater drag but drag does not grow proportional to the plate length. It is so because the boundary layer is thinner near the leading edge of the plate, where the plat experiences most drag. The drag is super-linear in the freestream fluid speed, growing as $U^{3/2}$, which is because the boundary layer thickness itself decreases with $U$ thereby enhancing the shear stress on the plate. Also, in the limit of vanishing viscosity, the drag decreases to zero proportional to $\sqrt{\mu}$. The physical explanation for why the drag might vanish with vanishing fluid density is left for the astute student to discover.
