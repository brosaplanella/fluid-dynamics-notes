figure(1), hold on
subplot('position',[.07 .1 .4 .4])

etaN = (2^(10)/3^5/pi^3)^(1/8);

eta = 0:.001:1;
plot(eta,((9*etaN^2/16)*(1-eta.^2)).^(1/3),'k-','linewidth',.8)
xlim([0 1.1]), ylim([0 .8])
xlabel('$\eta/\eta_N$','interpreter','latex','fontsize',12)
ylabel('$H(\eta)$','interpreter','latex','fontsize',12)

subplot('position',[.58 .1 .4 .4]), hold on
xlabel('$r/(gV^3/\nu)^{1/8}$','interpreter','latex','fontsize',12)
ylabel('$h/(\nu V/g)^{1/4}$','interpreter','latex','fontsize',12)
t = logspace(0,1,6);
R=etaN*t.^(1/8);
for j = 1:length(t);
    r = 0:0.001:R(j);
    c = [(R(j)-R(1))./(R(end)-R(1)) 0 (R(end)-R(j))./(R(end)-R(1))];
    plot([r R(j)],[t(j).^(-1/4)*((9*etaN^2/16)*(1-r.^2/R(j).^2)).^(1/3) 0],'-',...
        'color',c)
end
ylim([0 .8])
text(.58,.58,'$t=1$','interpreter','latex','fontsize',12)
text(.4,.31,'$t=10$','interpreter','latex','fontsize',12)
box on