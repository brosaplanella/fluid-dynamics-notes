close all
clear
figure(1), hold on

x=0:0.01:2;
mus=1; mup=3;
plot([0 2],[0 0],'k-')
plot(x,1+1./(1-x.^2),'b','linewidth',.7)
plot([1 1],[-50 50],'b--','linewidth',.7)
set(gca,'xticklabels',[],'yticklabels',[])
ylim([-40 40])
text(.6,-20,'$\dot{\varepsilon}=1/2\lambda \, \nearrow$','interpreter',...
    'latex','fontsize',16)
xlabel('$\dot{\varepsilon}$','interpreter','latex','fontsize',16)
ylabel('$\mu_{ext}$','interpreter','latex','fontsize',16)
box on