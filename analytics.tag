<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-177825517-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-177825517-1');
</script>
<script data-goatcounter="https://shreyasmandre.goatcounter.com/count"
                        async src="//gc.zgo.at/count.js"></script>
